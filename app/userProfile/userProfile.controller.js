/**
 * @ngdoc controller
 * @name angularApp.controller:UserProfileController
 * @description
 * # UserProfileController
 * UserProfile Controller loads user profile data
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('UserProfileController', UserProfileController);

    UserProfileController.$inject = ['UserProfileService', 'StorageUtil', '$location', '$loading', '$stateParams', '$q', '$scope', '$state', '$filter', '$timeout', 'UserAuthService', 'toastr'];

    function UserProfileController(UserProfileService, StorageUtil, $location, $loading, $stateParams, $q, $scope, $state, $filter, $timeout, UserAuthService, toastr) {
    	var vm = this;        
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
		//var userId = ($stateParams.userId) ? $stateParams.userId : angular.element('body').data('id');
		var userId = ($stateParams.userId) ? $stateParams.userId : ftoken;
		if (!userId || userId == '') {
			window.location.href='index.html';
		}
        //vm.isLogin = angular.element('body').data('islogin');
        vm.isSelf = (userId == ftoken) ? true : false;
		vm.currentLastCheckin = 0;
		vm.lastLastCheckin = -1;
		vm.currentLastWatch = 0;
		vm.lastLastWatch = -1;
		vm.currentLastFollowing = 0;
		vm.lastLastFollowing = -1;
		vm.currentLastFollowed = 0;
		vm.lastLastFollowed = -1;
		vm.isStillLoading = false;
		vm.userReviewObj = {
			movie_name: '',
			release_year: '',
            review_rating: 0,
            review_text: ''
        };
		vm.activeTab = 'ratedTab';
		
		//console.log("userId:" + userId);
		
        vm.loadUserProfileData = function() {
            
            $loading.start('commonLoader');
            var promises = [
                UserProfileService.getUserProfile(ftoken, sessionId, userId),
                UserProfileService.getUserStats(ftoken, sessionId, userId),
                UserProfileService.getUserCheckin(ftoken, sessionId, userId, 0),
                UserProfileService.getUserWatchList(ftoken, sessionId, userId, 0),
                UserProfileService.getPeopleFollowing(ftoken, sessionId, userId, 0),
                UserProfileService.getPeopleFollowed(ftoken, sessionId, userId, 0),
				UserProfileService.getUserReviews(ftoken, sessionId, userId),
            ];
            if (vm.isLogged === true && userId != ftoken) { /* Different users */
				promises.push(UserProfileService.getAllPeopleFollowed(ftoken, sessionId, ftoken, 0));
			}	
			
            $q.all(promises).then(function(results) {
                vm.userProfile = results[0].RESPONSE;
                vm.userStats = results[1].RESPONSE;
                vm.userCheckin = results[2].RESPONSE;
				vm.currentLastCheckin = vm.userCheckin.length;	
                vm.userWatchList = results[3].RESPONSE;
				vm.currentLastWatch = vm.userWatchList.length;		
                vm.peopleFollowing = results[4].RESPONSE; /* Get People Following User */
				vm.currentLastFollowing = vm.peopleFollowing.length;	
                vm.peopleFollowed = results[5].RESPONSE; /* Get People Followed by User */
				for (var i = 0; i < vm.peopleFollowed.length; i++) {
					if (vm.isLogged === true) {
						vm.peopleFollowed[i].is_followed = "1";
					} else {
						vm.peopleFollowed[i].is_followed = "0";
					}
				}
				vm.currentLastFollowed = vm.peopleFollowed.length;
                vm.userReviews = results[6].RESPONSE;   
				vm.linkReviewsWithCheckins();
				if (vm.isLogged === true && userId != ftoken) { /* Different users */
					vm.allPeopleFollowed = results[7].RESPONSE;
					vm.linkPeopleWithFollowed();
				}
				$('#profilePictureId').addClass('p' + vm.userStats.profile_score);
                $loading.finish('commonLoader');                 
            }, function(error){
                $loading.finish('commonLoader');
            });                    
        };

		vm.loadMore = function() {
			if (vm.isStillLoading !== true) { // only do next page if prev one has loaded 
				if (vm.activeTab == 'ratedTab') {
					if (vm.currentLastCheckin > vm.lastLastCheckin) { // check if there were any results last time
						vm.lastLastCheckin = vm.currentLastCheckin;
						//console.log("loadMorecheckin:" + vm.currentLastCheckin);
						$loading.start('commonLoader');
						vm.isStillLoading = true;
						UserProfileService.getUserCheckin(ftoken, sessionId, userId, vm.currentLastCheckin).then(function(result) {
							vm.userCheckin = vm.userCheckin.concat(result.RESPONSE);   
							vm.currentLastCheckin = vm.userCheckin.length;	
							vm.linkReviewsWithCheckins();
							vm.isStillLoading = false;
							$loading.finish('commonLoader');                 
						}, function(error){
							vm.isStillLoading = false;
							$loading.finish('commonLoader');
						});  
					}
				} else if (vm.activeTab == 'watchListTab') {
					if (vm.currentLastWatch > vm.lastLastWatch) { // check if there were any results last time
						vm.lastLastWatch = vm.currentLastWatch;
						//console.log("loadMorewatchlist:" + vm.currentLastWatch);
						$loading.start('commonLoader');
						vm.isStillLoading = true;
						UserProfileService.getUserWatchList(ftoken, sessionId, userId, vm.currentLastWatch).then(function(result) {
							vm.userWatchList = vm.userWatchList.concat(result.RESPONSE);   
							vm.currentLastWatch = vm.userWatchList.length;				
							vm.isStillLoading = false;
							$loading.finish('commonLoader');                 
						}, function(error){
							vm.isStillLoading = false;
							$loading.finish('commonLoader');
						});  
					}
				} else if (vm.activeTab == 'followingTab') { /* Get People Following User */
					if (vm.currentLastFollowing > vm.lastLastFollowing) { // check if there were any results last time
						vm.lastLastFollowing = vm.currentLastFollowing;
						//console.log("loadMorefollowing:" + vm.currentLastFollowing);
						$loading.start('commonLoader');
						vm.isStillLoading = true;
						UserProfileService.getPeopleFollowing(ftoken, sessionId, userId, vm.currentLastFollowing).then(function(result) {
							vm.peopleFollowing = vm.peopleFollowing.concat(result.RESPONSE);   
							vm.currentLastFollowing = vm.peopleFollowing.length;		
							if (vm.isLogged === true && userId != ftoken) { /* Different users */
								vm.linkPeopleWithFollowed();
							}
							vm.isStillLoading = false;
							$loading.finish('commonLoader');                 
						}, function(error){
							vm.isStillLoading = false;
							$loading.finish('commonLoader');
						});  
					}
				} else if (vm.activeTab == 'followedTab') { /* Get People Followed by User */
					if (vm.currentLastFollowed > vm.lastLastFollowed) { // check if there were any results last time
						vm.lastLastFollowed = vm.currentLastFollowed;
						//console.log("loadMorefollowed:" + vm.currentLastFollowed);
						$loading.start('commonLoader');
						vm.isStillLoading = true;
						UserProfileService.getPeopleFollowed(ftoken, sessionId, userId, vm.currentLastFollowed).then(function(result) {
							for (var i = 0; i < result.RESPONSE.length; i++) {
								result.RESPONSE[i].is_followed = "1";
							}
							vm.peopleFollowed = vm.peopleFollowed.concat(result.RESPONSE);   
							vm.currentLastFollowed = vm.peopleFollowed.length;	
							if (vm.isLogged === true && userId != ftoken) { /* Different users */
								vm.linkPeopleWithFollowed();
							}							
							vm.isStillLoading = false;
							$loading.finish('commonLoader');                 
						}, function(error){
							vm.isStillLoading = false;
							$loading.finish('commonLoader');
						});  
					}
				}				
			}
        };
		
		vm.linkReviewsWithCheckins = function() {
			for (var i = 0; i < vm.userCheckin.length; i++) {
				if (vm.userCheckin[i].review_text) { // skip the already found review
					continue;
				}
				
				for (var j = 0; j < vm.userReviews.length; j++) {
					if (vm.userReviews[j].movie_id == vm.userCheckin[i].movie_id) {
						vm.userCheckin[i].review_text = vm.userReviews[j].review_text;
						vm.userCheckin[i].review_id = vm.userReviews[j].review_id;
						vm.userCheckin[i].review_likes = vm.userReviews[j].review_likes;
						//console.log("match found");
						break;
					}
				}
			}
		}
		
		vm.linkPeopleWithFollowed = function() {
			for (var i = 0; i < vm.peopleFollowing.length; i++) {
				var found = false;
				for (var j =0; j < vm.allPeopleFollowed.length; j++) {
					if (vm.peopleFollowing[i].person_id == vm.allPeopleFollowed[j].person_id) {
						found = true;
						break;
					}
				}
				if (found === true) {
					vm.peopleFollowing[i].is_followed = "1";
				} else {
					vm.peopleFollowing[i].is_followed = "0";
				}
			}
			
			for (var i = 0; i < vm.peopleFollowed.length; i++) {
				var found = false;
				for (var j =0; j < vm.allPeopleFollowed.length; j++) {
					if (vm.peopleFollowed[i].person_id == vm.allPeopleFollowed[j].person_id) {
						found = true;
						break;
					}
				}
				if (found === true) {
					vm.peopleFollowed[i].is_followed = "1";
				} else {
					vm.peopleFollowed[i].is_followed = "0";
				}
			}
		}
		
        vm.openReviewBox = function(movie_name, year, review_text, rating) {
			vm.isReadOnly = true;
			vm.userReviewObj.movie_name = movie_name;
			vm.userReviewObj.release_year = year;
            vm.userReviewObj.review_rating = rating;
            vm.userReviewObj.review_text = review_text;
            angular.element('#writeReview').modal('show');
        };

		vm.addUserReviewLike = function(reviewId) {
			$loading.start('commonLoader');
            UserProfileService.addUserReviewLike(ftoken, sessionId, reviewId).then(function(result) {
                $loading.finish('commonLoader');
				//console.log(result.RESPONSE);
                toastr.success(result.RESPONSE);               
            });
		};
		
		vm.openCommentBox = function(reviewId) {
			//console.log("Opening commentbox reviewId:" + reviewId);
			angular.element('#addCommentBox-' + reviewId).toggleClass('commentBox');
		}
		
		vm.addUserReviewComment = function(reviewId) {
			var commentCheckin = $filter('filter')(vm.userCheckin, {
					review_id: reviewId
				})[0];
			var commentText = commentCheckin.comment;
			commentCheckin.comment = '';			
			//console.log("commentText:" + commentText);
			if (commentText != "") {
				$loading.start('commonLoader');
				UserProfileService.addUserReviewComment(ftoken, sessionId, reviewId, userId, commentText ).then(function(result) {
					$loading.finish('commonLoader');
					angular.element('#addCommentBox-' + reviewId).addClass('commentBox');
					//console.log(result.RESPONSE);
					toastr.success(result.RESPONSE);               
				});
			}
		};
		
		vm.shareUserReview = function(reviewId) {
			$loading.start('commonLoader');
            UserProfileService.shareUserReview(ftoken, sessionId, reviewId).then(function(result) {
                $loading.finish('commonLoader');
				//console.log(result.RESPONSE);
                toastr.success(result.RESPONSE);                 
            });
		};
		
        vm.toggleFollowPerson = function(isFollowed,personId) {
			if (personId == "5e21e06dd861104d09e5" || personId == ftoken) {return;} // main flickstree account or your account
            $loading.start('commonLoader');
			var actionType = "follow";
			if (isFollowed == "1") {
				actionType = "leave";
			}
            UserProfileService.followLeavePerson(ftoken, sessionId, personId, actionType).then(function() {
				// find out the person to update
				var personFollowing = $filter('filter')(vm.peopleFollowing, {
					person_id: personId
				})[0];
				//console.log("personFollowing:" + JSON.stringify(personFollowing));
				if (angular.isDefined(personFollowing)) {
					personFollowing.is_followed = (isFollowed == "0") ? "1" : "0";
				}
				var personFollowed = $filter('filter')(vm.peopleFollowed, {
					person_id: personId
				})[0];
				//console.log("personFollowed:" + JSON.stringify(personFollowed));
				if (angular.isDefined(personFollowed)) {
					personFollowed.is_followed = (isFollowed == "0") ? "1" : "0";
				}
				if (vm.userProfile.user_id == personId) {
					vm.userProfile.is_followed = (isFollowed == "0") ? "1" : "0";
				}
                $loading.finish('commonLoader');   
            });
        };

        vm.routeToEditProfile = function() {
            $state.go('user-profile-edit', {userId: userId}, { location: false });
        };
		
		$scope.clickOnUpload = function (e) {
			//console.log("clickOnUpload");
			angular.element('#fileInput').triggerHandler('click');
		};
		
        vm.submitUserForm = function(userForm) {
            if(userForm.$valid) {
                $loading.start('commonLoader');
                UserProfileService.updatePassword(ftoken, sessionId, vm.userObj.oldPassword, vm.userObj.newPassword).then(function(result) {
                    $loading.finish('commonLoader');   
					toastr.success(result.RESPONSE);  
					if (result.RESULT == "SUCCESS") {
						$state.go('user-profile', {userId: userId}, { location: false });
					}
                });
            }
        };

		vm.makeActiveTab = function(tabId) {
			$('.reviewsValue').removeClass('active');
			$('#' + tabId).addClass('active');
			vm.activeTab = tabId;
			//angular.element($event.currentTarget.parent).addClass('active');
		}
		
        $scope.fileChanged = function(e) {          
			//console.log("fileChanged");
            var files = e.target.files;
        
            var fileReader = new FileReader();
            fileReader.readAsDataURL(files[0]);     
            
            fileReader.onload = function(e) {
                $scope.imgSrc = this.result;
                $scope.$apply();
				//console.log("showing myPictureModal");
                angular.element('#myPictureModal').modal('show');
            };            
        }; 

		$scope.uploadFile = function() {
              var formData = new FormData();
              //$scope.initCrop = true;
			  if ($scope.resultBlob) {
				  $loading.start('commonLoader');
				  //console.log("resultBlob:" + $scope.resultBlob);
				  //console.log("resultBlob:" + $scope.result);
				  formData.append('image_file', $scope.resultBlob);

				  $.ajax('https://www.flickstree.com/api_service_130v.php?VERSION=1.30&ENCODING=JSON&FTOKEN=' + ftoken + '&SESSION=' + sessionId + '&METHOD=UPLOAD_USER_PICTURE&user_id=' + userId, {
					method: "POST",
					data: formData,
					processData: false,
					contentType: false,
					success: function (result) {
					  //console.log('Upload success:' + result);
					  angular.element('#myModal').modal('hide');
					  $loading.finish('commonLoader');
					  //$state.go('user-profile', {}, { location: false });
					  location.reload(true);
					},
					error: function () {
					  console.log('Upload error');
					  $loading.finish('commonLoader');
					}
				  });
			  } else {
				  console.log('No Image yet');
			  }
        };

        vm.Date = function(date) {
			if (angular.isDefined(date) && date != null) {
				return new Date(date.replace(/-/g, '/'));
			} else {
				return new Date(date);
			}
        };

        vm.range = function(n) {
            return new Array(parseInt(n));
        };

		vm.getFormattedRating = function(rating) {
            if(rating && rating > 0) {
                return parseFloat(rating/2).toFixed(1);
            } else {
                return '--';
            }
        };
		
        vm.loadMovieTrailer = function(movie_trailer) {
            vm.loadVideo = true;
            vm.movie_trailer = movie_trailer;

            angular.element('#introVideo').modal('show');
            angular.element('#introVideo').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
                vm.loadVideo = false;
            });
        };

        vm.loadUserProfileData();
    }

})();
