 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider        
        .state('user-profile', {
            url: 'user-profile.html/:userId',
            templateUrl: 'app/userProfile/userProfile.html',
            controller: 'UserProfileController',
            controllerAs: 'UserProfileCtrl'
        })
        .state('user-profile-edit', {
            url: 'user-profile.html/:userId',
            templateUrl: 'app/userProfile/userProfile_form.html',
            controller: 'UserProfileController',
            controllerAs: 'UserProfileCtrl'
        });
        
        $urlRouterProvider.otherwise(function($injector, $location) {
          var $state = $injector.get('$state');      
			$state.go('user-profile', {}, { location: false });
            if($location.path() && $location.path().split('/') && $location.path().split('/').length > 2) {
                $state.go('user-profile', {userId: $location.path().split('/')[2]}, { location: false });
            } else {
                $state.go('user-profile', {}, { location: false });
            }
        });
    }
	
	angular
        .module('angularApp')
        .config(function(toastrConfig) {
		  angular.extend(toastrConfig, {
			positionClass: 'toast-bottom-center',
		  });
		});

})();