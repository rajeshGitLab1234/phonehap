/**
 * @ngdoc service
 * @name angularApp.service:UserProfileService
 * @description
 * # UserProfileService
 * UserProfile Service for User controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('UserProfileService', UserProfileService);

    UserProfileService.$inject = ['$http', '$q', 'CONSTANTS'];

    function UserProfileService($http, $q, CONSTANTS) {

        var service = {            
            getUserProfile: getUserProfile,
            followLeavePerson: followLeavePerson,
            getUserCheckin: getUserCheckin,
			getUserReviews: getUserReviews,
            getUserWatchList: getUserWatchList,
            getPeopleFollowing: getPeopleFollowing,
            getPeopleFollowed: getPeopleFollowed,
            getUserStats: getUserStats,
            updatePassword: updatePassword,
			addUserReviewLike: addUserReviewLike,
			addUserReviewComment: addUserReviewComment,
			shareUserReview: shareUserReview,
			getAllPeopleFollowed: getAllPeopleFollowed
        };

        return service;

        function getUserProfile(ftoken, sessionId, userId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_USER_PROFILE&user_id='+userId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUserProfile", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getUserStats(ftoken, sessionId, userId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+userId+'&SESSION='+sessionId+'&METHOD=GET_USER_STATS&user_id='+userId;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUserStats", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
        
        function getUserCheckin(ftoken, sessionId, userId, startFrom) {

            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_USER_CHECKIN&user_id='+userId+'&start_from='+startFrom;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUserCheckin", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

		function getUserReviews(ftoken, sessionId, userId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_USER_REVIEWS&max_results=1000&user_id='+userId;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUserReviews", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }
		
        function getUserWatchList(ftoken, sessionId, userId, startFrom) {

            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_USER_WATCHLIST&user_id='+userId+'&start_from='+startFrom;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUserWatchList", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

		/* Get People Following User */
        function getPeopleFollowing(ftoken, sessionId, userId, startFrom) {

            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_PEOPLE_FOLLOWING&user_id='+userId+'&start_from='+startFrom;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getPeopleFollowing", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

		/* Get People Followed by User */
        function getPeopleFollowed(ftoken, sessionId, userId, startFrom) {

            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_PEOPLE_FOLLOWED&user_id='+userId+'&start_from='+startFrom;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getPeopleFollowed", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

		/* Get All People Followed by User */
        function getAllPeopleFollowed(ftoken, sessionId, userId, startFrom) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_PEOPLE_FOLLOWED&user_id='+userId+'&max_results=1000';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getPeopleFollowed", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }
		
        function updatePassword(ftoken, sessionId, old_password, new_password) {
            
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=UPDATE_USER_PASSWORD'+'&old_password='+old_password+'&new_password='+new_password;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("updatePassword", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function followLeavePerson(ftoken, sessionId, personId, actionType) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&person_id='+personId;
			if (actionType == "follow") {
				url += '&METHOD=FOLLOW_PERSON';
			} else if (actionType == "leave") {
				url += '&METHOD=LEAVE_PERSON';
			}
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("followLeavePerson", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		function addUserReviewLike(ftoken, sessionId, reviewId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=ADD_LIKE&like_type=review&like_id='+reviewId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("addUserReviewLike", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		function addUserReviewComment(ftoken, sessionId, reviewId, reviewerId,commentText) {
			var postId = 'REVIEW-' + reviewId;
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=ADD_COMMENT&like_type=review&post_id='+postId+'&post_owner_id='+reviewerId+'&comment='+commentText;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("addUserReviewComment", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
        
		function shareUserReview(ftoken, sessionId, reviewId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=SHARE_USER_REVIEW&review_id='+reviewId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareUserReview", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();