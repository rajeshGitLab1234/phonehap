 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider
        
        .state('critics-profile', {
            url: 'critics-profile.html/:criticid',
            templateUrl: 'app/criticsProfileFollow/criticsProfileFollow.html',
            controller: 'CriticsProfileFollowController',
            controllerAs: 'CriticsProfileFollowCtrl'
        });
        
        $urlRouterProvider.otherwise(function($injector, $location) {
          var $state = $injector.get('$state');          
            $state.go('critics-profile', {}, { location: false });
            if($location.path() && $location.path().split('/') && $location.path().split('/').length > 2) {
                $state.go('critics-profile', {criticid: $location.path().split('/')[2]}, { location: false });
            } else {
                $state.go('critics-profile', {}, { location: false });
            }
        });
    }
	
	angular
        .module('angularApp')
        .config(function(toastrConfig) {
		  angular.extend(toastrConfig, {
			positionClass: 'toast-bottom-center',
		  });
		});

})();