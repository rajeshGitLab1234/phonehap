/**
 * @ngdoc service
 * @name angularApp.service:CriticsProfileFollowService
 * @description
 * # CriticsProfileFollowService
 * CriticsProfileFollow Service for CriticsProfileFollow controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('CriticsProfileFollowService', CriticsProfileFollowService);

    CriticsProfileFollowService.$inject = ['$http', '$q', 'CONSTANTS'];

    function CriticsProfileFollowService($http, $q, CONSTANTS) {

        var service = {            
            getCriticsProfile: getCriticsProfile,
            getCriticsReview: getCriticsReview,
			followLeaveCritic: followLeaveCritic,
            shareMovie: shareMovie,
			shareReview: shareReview
        };

        return service;

        function getCriticsProfile(ftoken, sessionId, criticId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_CRITIC_PROFILE&critic_id='+criticId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCritcisProfile", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getCriticsReview(ftoken, sessionId, criticId, startFrom) {
			//var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_CRITIC_REVIEWS&critic_id='+criticId + '&start_from=' + startFrom;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCriticsReview", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		function followLeaveCritic(ftoken, sessionId, criticId, actionType) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&critic_id='+criticId;
			if (actionType == "follow") {
				url += '&METHOD=FOLLOW_CRITIC';
			} else if (actionType == "leave") {
				url += '&METHOD=LEAVE_CRITIC';
			}
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("followLeaveCritic", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function shareMovie(sessionId, ftoken, movieId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=SHARE_MOVIE&movie_id='+movieId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareMovie", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		function shareReview(sessionId, ftoken, reviewId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=SHARE_CRITIC_REVIEW&review_id='+reviewId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareReview service data:", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();