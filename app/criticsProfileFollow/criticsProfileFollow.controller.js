/**
 * @ngdoc controller
 * @name angularApp.controller:CriticsProfileFollowController
 * @description
 * # CriticsProfileFollowController
 * CriticsProfileFollow Controller loads home page data
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('CriticsProfileFollowController', CriticsProfileFollowController);

    CriticsProfileFollowController.$inject = ['CriticsProfileFollowService', 'StorageUtil', '$location', '$loading', '$stateParams', 'UserAuthService', 'toastr'];

    function CriticsProfileFollowController(CriticsProfileFollowService, StorageUtil, $location, $loading, $stateParams, UserAuthService, toastr) {
    	var vm = this;   
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
		
        var criticId = ($stateParams.criticid) ? $stateParams.criticid : angular.element('body').data('id');
		vm.isStillLoading = false;
		vm.currentReview = 0;
		vm.lastReview = -1;
		
        vm.loadCriticProfileData = function() {
            $loading.start('commonLoader');
            CriticsProfileFollowService.getCriticsProfile(ftoken, sessionId, criticId).then(function(criticProfileResult) {
                CriticsProfileFollowService.getCriticsReview(ftoken, sessionId, criticId, 0).then(function(criticReviewResult) {
                    vm.criticProfile = criticProfileResult.RESPONSE;
                    vm.criticReviews = criticReviewResult.RESPONSE;     
					vm.currentReview = vm.criticReviews.length;
					vm.lastReview = 0;
                    $loading.finish('commonLoader');                 
                }, function(error){
                    $loading.finish('commonLoader');
                }); 
            }, function(error){
                $loading.finish('commonLoader');
            });                       
        };

		vm.loadMoreCriticReviews = function(startFrom) {
            $loading.start('commonLoader');
			vm.isStillLoading = true;
			CriticsProfileFollowService.getCriticsReview(ftoken, sessionId, criticId, startFrom).then(function(criticReviewResult) {
				vm.criticReviews = vm.criticReviews.concat(criticReviewResult.RESPONSE);   
				vm.currentReview = vm.criticReviews.length;				
				vm.isStillLoading = false;
				$loading.finish('commonLoader');                 
			}, function(error){
				vm.isStillLoading = false;
				$loading.finish('commonLoader');
			});                      
        };
		
		vm.loadMoreReviews = function() {
			if (vm.isStillLoading !== true) { // only do next page if prev one has loaded 
				if (vm.currentReview > vm.lastReview) { // check if there were any results last time
					vm.lastReview = vm.currentReview;
					//console.log("loadMoreReviews:" + vm.currentReview);
					vm.loadMoreCriticReviews(vm.currentReview);
				}
			}
        };
		
        vm.addFollowCritic = function() {
            $loading.start('commonLoader');
            CriticsProfileFollowService.followCritic(ftoken, sessionId, criticId).then(function(response) {
				toastr.success(response.RESPONSE);
                $loading.finish('commonLoader');   
            });
        };

		vm.manageFollowCritic = function(actionType) {
            $loading.start('commonLoader');
            CriticsProfileFollowService.followLeaveCritic(ftoken, sessionId, criticId, actionType).then(function(response) {
				if (actionType == 'follow') {
					vm.criticProfile.is_followed = "1";
				} else {
					vm.criticProfile.is_followed = "0";
				}
				toastr.success(response.RESPONSE);
                $loading.finish('commonLoader');   
            });
        };
		
        vm.Date = function(date) {
			if (angular.isDefined(date) && date != null) {
				return new Date(date.replace(/-/g, '/'));
			} else {
				return new Date(date);
			}
        };

        vm.range = function(n) {
            return new Array(parseInt(n));
        };

        vm.shareMovie = function(movieId) {
            $loading.start('commonLoader');
            CriticsProfileFollowService.shareMovie(sessionId, ftoken, movieId).then(function(result) {
                $loading.finish('commonLoader');
				//console.log(result.RESPONSE);
				toastr.success(response.RESPONSE);
            });
        };

		vm.shareReview = function(reviewId) {
            $loading.start('commonLoader');
            CriticsProfileFollowService.shareReview(sessionId, ftoken, reviewId).then(function(result) {
                $loading.finish('commonLoader');
				//console.log(result.RESPONSE);
				toastr.success(response.RESPONSE);
            });
        };
		
        vm.loadMovieTrailer = function(movie_trailer) {
            vm.loadVideo = true;
            vm.movie_trailer = movie_trailer;
            angular.element('#introVideo').modal('show');
            angular.element('#introVideoHome').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
            });
        };

        vm.loadCriticProfileData(vm.currentReview);
    }

})();
