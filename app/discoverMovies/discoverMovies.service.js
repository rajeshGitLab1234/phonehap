/**
 * @ngdoc service
 * @name angularApp.service:DiscoverMoviesService
 * @description
 * # DiscoverMoviesService
 * DiscoverMovies Service for DiscoverMovies controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('DiscoverMoviesService', DiscoverMoviesService);

    DiscoverMoviesService.$inject = ['$http', '$q', 'CONSTANTS'];

    function DiscoverMoviesService($http, $q, CONSTANTS) {

        var service = {            
            getAdvancedSearchMovies: getAdvancedSearchMovies,
            getRecommendedMoviesData: getRecommendedMoviesData,
            postReview: postReview,
            getCriticsRecommendedMoviesData: getCriticsRecommendedMoviesData,
            clearRecommendation: clearRecommendation,
			checkNewRecommendation: checkNewRecommendation,
			getCheckins: getCheckins,
			searchGenre: searchGenre
        };

        return service;

        function getAdvancedSearchMovies(ftoken, advanceSearchObj, startFrom) {
            //var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
			startFrom = (startFrom) ? startFrom : "0";
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&METHOD=ADVANCE_SEARCH_MOVIES&start_from='+startFrom+'&max_results=30';
            var deferred = $q.defer();
            
            if(advanceSearchObj.movie_genre && advanceSearchObj.movie_genre.length >0) {
                url += "&movie_genre=" + advanceSearchObj.movie_genre.join('@');
            }

            if(advanceSearchObj.movie_language && advanceSearchObj.movie_language.length >0) {
                url += "&movie_language=" + advanceSearchObj.movie_language.join('@');
            }

            if(advanceSearchObj.movie_decade && advanceSearchObj.movie_decade != '') {
                if(advanceSearchObj.movie_decade == '1950-2000') {
                    url += "&movie_decade=" + '1950@1960@1970@1980@1990';
                } else if(advanceSearchObj.movie_decade == '2000-2010') {
                    url += "&movie_decade=" + '2000';
                } else if(advanceSearchObj.movie_decade == '2010-2016') {
                    //var currentYear = new Date().getFullYear();
                    url += "&movie_decade=" + '2010';
                }                
            }

            if(advanceSearchObj.watch_available && advanceSearchObj.watch_available != "") {
                url += "&watch_available=" + advanceSearchObj.watch_available;
            }
			
			//console.log("getAdvancedSearchMovies URL:" + url);
			
            $http.get(url).then(function(response) {
                //console.log("getAdvancedSearchMovies", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getRecommendedMoviesData(sessionId, ftoken, startFrom, getNew) {
			getNew = (getNew) ? getNew : "";
			startFrom = (startFrom) ? startFrom : "0";
            //var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_RECOMMENDATIONS&start_from='+startFrom+'&max_results=30&get_new=' + getNew;
			//console.log("getRecommendedMoviesData URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getRecommendedMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getCriticsRecommendedMoviesData(sessionId, ftoken, startFrom) {
            //var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
			startFrom = (startFrom) ? startFrom : "0";
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_CRITICS_RECOMMENDED_MOVIES_ADVANCED&start_from='+startFrom+'&max_results=30';
			//console.log("getCriticsRecommendedMoviesData URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCriticsRecommendedMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		// Post Review or Add Checkin
		function postReview(sessionId, ftoken, movieId, reviewObj) {
			var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId;
			if (reviewObj.text.length > 0) {
				var reviewText = reviewObj.text.replace(/\s/g, "+");
				url += '&METHOD=ADD_USER_REVIEW&movie_id='+movieId+'&review_title='+reviewText+'&review_text='+reviewText+'&rating='+reviewObj.rating;
			} else {
				url += '&METHOD=ADD_CHECKIN&movie_id='+movieId+'&checkin_rating='+reviewObj.rating;
			}
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("postReview", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise; 
        }
		
        function clearRecommendation(ftoken, sessionId, movieId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=CLEAR_RECOMMENDATION&movie_id='+movieId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("clearRecommendation", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise; 
        } 
		
		function checkNewRecommendation(ftoken, sessionId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_RECOMMENDATIONS&check_new=Y';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("checkNewRecommendation", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise; 
        } 

		function getCheckins(sessionId, ftoken) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_USER_CHECKIN&max_results=1000';
			//console.log("getCheckins URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCheckins", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		function searchGenre(searchTerm) {
			var url = CONSTANTS.API_URL+'&METHOD=SEARCH_RECOMMEND_GENRE&start_from=0&max_results=5&search_term='+searchTerm;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchGenre", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
	}

})();
