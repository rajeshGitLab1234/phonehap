/**
 * @ngdoc controller
 * @name angularApp.controller:DiscoverMoviesController
 * @description
 * # DiscoverMoviesController
 * DiscoverMovies Controller loads movie listing
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('DiscoverMoviesController', DiscoverMoviesController);

    DiscoverMoviesController.$inject = ['DiscoverMoviesService', 'StorageUtil', '$location', '$loading', '$stateParams', '$q', '$state', '$filter', '$window', 'CONSTANTS', 'UserAuthService','toastr'];

    function DiscoverMoviesController(DiscoverMoviesService, StorageUtil, $location, $loading, $stateParams, $q, $state, $filter, $window, CONSTANTS, UserAuthService, toastr) {
    	var vm = this;
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
		vm.currentLastMovie = 0;
		vm.lastLastMovie = -1;
        vm.loadVideo = false;
        vm.movies = [];
        vm.reviewObj = {
            rating: 0,
            text: '',
            movie_id: ''
        };
        vm.isSeachBoxOpen = false;
        vm.viewType = angular.element('body').data('type');
        vm.advanceSearchObj = {
            movie_language: [],
            movie_genre: [],
            movie_cast: [],
            movie_decade: '',
            movie_release_date_start: '',
            movie_release_date_end: '',
            watch_available:''
        };
		vm.isStillLoading = false;
		vm.newRecommendationAvailable = false;
		vm.searchGenreList = [];
		vm.presetGenres = ['Romance','Action','Horror','Drama','Animation','Sci-Fi','Thriller','History','Comedy'];
		
		//vm.checkinArray = [];
		
        var storageAdvanceSearch = StorageUtil.getLocalObject('searchObj');

        if(storageAdvanceSearch) {
            vm.advanceSearchObj = storageAdvanceSearch;
        } else if(vm.viewType == 'search') {
			vm.isSeachBoxOpen = true;
		}

        vm.loadMovies = function(startFrom,getNew) {
            
            $loading.start('commonLoader');
			vm.isStillLoading = true;
            var promises = [];
            if(vm.viewType == 'search') {
                promises.push(DiscoverMoviesService.getAdvancedSearchMovies(ftoken, vm.advanceSearchObj, startFrom));
            } else if(vm.viewType == 'criticsRecommended') {
                promises.push(DiscoverMoviesService.getCriticsRecommendedMoviesData(sessionId, ftoken, startFrom));
            } else { //recommended
				if (vm.isLogged) { // logged in
					promises.push(DiscoverMoviesService.getRecommendedMoviesData(sessionId, ftoken, startFrom, getNew));    
				} else { // not logged in, redirect to main page
					//window.location.href='index.html';
					$window.location.href = CONSTANTS.SITE_LINK;
				}					
            } 

            if(promises && promises.length >0) {
                $q.all(promises).then(function(result) {
					////console.log("startFrom:" + startFrom);
					if (startFrom > 0) { // it is subsequent page, so append results
						vm.movies = vm.movies.concat(result[0].RESPONSE);
					} else { // if it is first page or reset of parameters, replace results
						vm.movies = result[0].RESPONSE;
						vm.lastLastMovie = -1;
					}		

					vm.currentLastMovie = vm.movies.length;	
					
					// Get Checkins
					DiscoverMoviesService.getCheckins(sessionId, ftoken).then(function(checkinResult) {
						for (var j = 0; j < checkinResult.RESPONSE.length; j++) {
							for (var i = 0; i < vm.movies.length; i++) {
								if (vm.movies[i].movie_id == checkinResult.RESPONSE[j].movie_id) {
									vm.movies[i].done_checkin = true;
									//console.log("match found");
									break;
								}
							}
						}
						vm.isStillLoading = false;
						$loading.finish('commonLoader');
					}, function(error){
						vm.isStillLoading = false;
						$loading.finish('commonLoader');
					});	
					
					if (vm.viewType == 'recommended') {
						// Check if user has actually got any recommendations
						if (vm.movies.length <= 0) { // no recommendations
							$window.location.href = CONSTANTS.SITE_LINK;
						}
						// Check New Recommendations
						DiscoverMoviesService.checkNewRecommendation(ftoken, sessionId).then(function(newRecommendResult) {
							//console.log("checkNewRecommendation:" + newRecommendResult.RESPONSE);
							if (newRecommendResult.RESPONSE && newRecommendResult.RESPONSE != '0') {
								//console.log("NewRecommendation available");
								vm.newRecommendationAvailable = true;
							} else {
								vm.newRecommendationAvailable = false;
							}
						}, function(error){
							//console.log("checkNewRecommendation failed");
						});      
					}						
                }, function(error){
					vm.isStillLoading = false;
                    $loading.finish('commonLoader');
                });
            }                        
        };

		vm.loadMoreMovies = function() {
			if (vm.isStillLoading !== true && vm.viewType != 'recommended') { // only do next page if prev one has loaded 
				if (vm.currentLastMovie > vm.lastLastMovie) { // check if there were any results last time
					vm.lastLastMovie = vm.currentLastMovie;
					//console.log("loadMoreMovies:" + vm.currentLastMovie);
					vm.loadMovies(vm.currentLastMovie);
				}
			}
        };
		
		vm.loadNewRecommendations = function() {
			if (vm.isStillLoading !== true) {
				vm.loadMovies(0,'Y');
			}
		}
		
        vm.Date = function(date) {
			if (angular.isDefined(date) && date != null) {
				return new Date(date.replace(/-/g, '/'));
			} else {
				return new Date(date);
			}
        };

        vm.loadMovieTrailer = function(movie_trailer) {
            vm.loadVideo = true;
            vm.movie_trailer = movie_trailer;

            angular.element('#introVideo').modal('show');
            angular.element('#introVideo').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
				vm.loadVideo = false;
            });
        };

        vm.routeToMovieDetail =  function(movie_id) {
			//console.log("discoverMovies controller routeToMovieDetail movie id:" + movie_id);
            $state.go('movie-detail', {'id': movie_id}, {reload: true});
        };
		
		vm.routeToPreferences =  function() {
			//$state.go('change-preference', {}, {reload: true});
            $window.location.href = CONSTANTS.SITE_LINK + CONSTANTS.PAGES.PREFERENCES;
        };

        vm.getFormattedRating = function(rating) {
            if(rating && rating > 0) {
                return parseFloat(rating/2).toFixed(1);
            } else {
                return '--';
            }
        };

		vm.createMovieCriterion = function(text) {
			var langArray = ['English','Hindi'];
			var genreArray = ["Family","History","Sci-Fi","Horror","Fantasy","Crime","Thriller","Animation","Mystery","Romance",
							"Comedy","Drama","War","Action","Musical","Sport","Spy","Epic","Kids","Adult","Experimental"];
			var result = "";
			// Get Language out 
			for (var i = 0; i < langArray.length; i++) {
				if (text.toLowerCase().indexOf(langArray[i].toLowerCase()) > -1) {
					//console.log("lang found");
					result += langArray[i] + ' ';
					var re = new RegExp(langArray[i],"i");
					text = text.replace(re, "");
					//break;
				}
			}
			//console.log(text);
			// Get Genre out
			for (i = 0; i < genreArray.length; i++) {
				if (text.toLowerCase().indexOf(genreArray[i].toLowerCase()) > -1) {
					//console.log("genre found");
					result += genreArray[i] + ' ';
					var re = new RegExp(genreArray[i],"i");
					text = text.replace(re, "");
					//break;
				}
			}
			//console.log(text);
			result += 'Movie ';
			// Get Decade
			var era = text.match(/\d{4}/);
			if (era) {
				//console.log("era found");
				var re = new RegExp(era[0],"i");
				text = text.replace(re, "");
			}
			//console.log(text);
			text = text.trim();		
			//console.log(text);			
			if (text.length > 1) {
				result += 'of ' + text + ' ';
			}
			if (era) {
				result += 'from ' + era[0] + 's'; 
			}
			return result.replace('|','');
		}
		
		vm.splitTextintoCSV = function(text) {
			var result = text.split(/ /).join(', ');
			result = result.substring(0,result.length - 2);
			return result;
		}
        vm.setAdvanceSearchOption = function($event, searchType, searchVal) {
            if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length>0 && vm.advanceSearchObj[searchType].indexOf(searchVal) > -1) {
                vm.advanceSearchObj[searchType].splice(vm.advanceSearchObj[searchType].indexOf(searchVal), 1);
                angular.element($event.currentTarget).removeClass('activeIcon');
            } else if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length < 3) {
                vm.advanceSearchObj[searchType].push(searchVal);   
                angular.element($event.currentTarget).addClass('activeIcon'); 
            }            
        };

		vm.setAdvanceSearchOptionString = function($event, searchType, searchVal) {
			//console.log("setAdvanceSearchOptionString, searchType:" + searchType + ",searchVal:" + searchVal + ",event:" + $event.currentTarget);
			if (searchType == "movie_decade"){
				$('.RELEASEYEAR').removeClass('activeIconBackground');
			} else if (searchType == "watch_available"){
				$('.AVAILABLEWATCHto').removeClass('activeIconBackground');
			}
            vm.advanceSearchObj[searchType] = searchVal;    
			angular.element($event.currentTarget).addClass('activeIconBackground'); 
        };
		
        vm.routeToDiscoverMovie = function(pageType) {
            StorageUtil.setLocalObject('searchObj', vm.advanceSearchObj); 
            vm.isSeachBoxOpen = false;       
            vm.currentLastMovie = 0;
            vm.loadMovies(vm.currentLastMovie);
        };

        vm.setRating = function(rating) {
            vm.reviewObj.rating = rating;
        };

        vm.showReviewBox = function(movie_id,done_checkin) {
			//console.log("done_checkin:" + done_checkin);
			if ((done_checkin && done_checkin !== true) || angular.isUndefined(done_checkin) || done_checkin == null) {
				//console.log("showing review box");
				vm.reviewObj.movie_id = movie_id;
				vm.currentMovie = $filter('filter')(vm.movies, {
					movie_id: movie_id
				})[0];
				//console.log("currentMovie:" + vm.currentMovie);
				angular.element('#writeReview').modal('show');
			}
        }

        vm.postReviewForm = function() {
            if(vm.reviewObj.rating == 0) {
                alert("please select rating.");
            } else {
                DiscoverMoviesService.postReview(sessionId, ftoken, vm.reviewObj.movie_id, vm.reviewObj).then(function(result) {
                    vm.reviewObj = {
                        rating: 0,
                        text: '',
                        movie_id: ''
                    };
					vm.currentMovie.done_checkin = true;
                    angular.element('#writeReview').modal('hide');
                }, function(error){
                    $loading.finish('commonLoader');
                });    
            }
        };

        vm.clearRecommendation = function(movie_id) {
            $loading.start('commonLoader');
            DiscoverMoviesService.clearRecommendation(ftoken, sessionId, movie_id).then(function(result) {
                vm.movies = [];
                vm.currentPage = 0;
				toastr.success("Movie deleted from your profile successfully. Similar movies won't be suggested in future.");
                vm.loadMovies(vm.currentPage);
            }, function(error){
                $loading.finish('commonLoader');
            }); 

        };

		vm.searchGenre = function($event) {
			vm.searchValue = $event.currentTarget.value; 
			if (vm.searchValue.length > 0) {
				DiscoverMoviesService.searchGenre(vm.searchValue).then(function(result) {  
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchGenreList = result.RESPONSE;
                        } else {
                            vm.searchGenreList = [];
                        }                        
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });  
			}
		};
		
		vm.clearSearch = function(searchType) {
			if (searchType == "genre") {
				angular.element('#searchGenreBox').val('');
				vm.searchGenreList = [];
			}
		}
		
        vm.loadMovies(vm.currentPage);
    }
})();
