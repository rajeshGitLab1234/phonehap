/**
 * @ngdoc service
 * @name angularApp.service:PopuplarCriticsService
 * @description
 * # PopuplarCriticsService
 * PopuplarCritics Service for PopuplarCritics controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('PopularCriticsService', PopularCriticsService);

    PopularCriticsService.$inject = ['$http', '$q', 'CONSTANTS'];

    function PopularCriticsService($http, $q, CONSTANTS) {

        var service = {            
            getCriticsData: getCriticsData,
			followLeaveCritic: followLeaveCritic            
        };

        return service;

        function getCriticsData(ftoken, language, startFrom) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&language='+language+'&METHOD=GET_CRITICS_BY_REVIEWS&start_from='+startFrom;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCriticsData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        } 
		
		function followLeaveCritic(sessionId, ftoken, criticId, actionType) {
			var url;
			if (actionType == "follow") {
				url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=FOLLOW_CRITIC&critic_id='+criticId;
			} else if (actionType == "leave"){
				url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=LEAVE_CRITIC&critic_id='+criticId;
			}
            //var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=FOLLOW_CRITIC&critic_id='+criticId;
			//console.log("URL:" + url);
			
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("followCritic", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();
