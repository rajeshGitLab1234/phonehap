/**
 * @ngdoc controller
 * @name angularApp.controller:PopularCriticsController
 * @description
 * # PopularCriticsController
 * PopularCritics Controller loads home page data
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('PopularCriticsController', PopularCriticsController);

    PopularCriticsController.$inject = ['PopularCriticsService', 'StorageUtil', '$location', '$loading', '$filter', 'UserAuthService', 'toastr'];

    function PopularCriticsController(PopularCriticsService, StorageUtil, $location, $loading, $filter, UserAuthService, toastr) {
    	var vm = this;
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
		vm.isStillLoading = false;
		vm.currentLastCritic = 0;
		vm.lastLastCritic = -1;
		vm.language = 'Hindi';
		
        vm.loadCritics = function(language,startFrom) {
            
            $loading.start('commonLoader');
			if (language) {
				vm.language = language;
			} else {
				language = vm.language;
			}
            //vm.language = (language) ? language : 'English';
			startFrom =  (startFrom) ? startFrom : '0';
            PopularCriticsService.getCriticsData(ftoken, language,startFrom).then(function(result) {
				if (startFrom == "0") {
					vm.critics = result.RESPONSE;
					vm.lastLastCritic = -1;
				} else {
					vm.critics = vm.critics.concat(result.RESPONSE);
				}
				vm.currentLastCritic = vm.critics.length;
                $loading.finish('commonLoader');              
            }, function(error){
                $loading.finish('commonLoader');
            });            
        };

		vm.loadMoreCritics = function() {
			if (vm.isStillLoading !== true) { // only do next page if prev one has loaded 
				if (vm.currentLastCritic > vm.lastLastCritic) { // check if there were any results last time
					vm.lastLastCritic = vm.currentLastCritic;
					//console.log("loadMoreCritics:" + vm.currentLastCritic);
					vm.loadCritics(vm.language,vm.currentLastCritic);
				}
			}
        };
		
		vm.followLeaveCritic = function($event,criticId,isFollowed) {
			if (isFollowed == "1") { // already followed, leave 
				$loading.start('commonLoader');
				PopularCriticsService.followLeaveCritic(sessionId, ftoken, criticId, "leave").then(function(response) {
					toastr.success(response.RESPONSE);
					//vm.critics[0].is_followed = "0";
					var selectedCritic = $filter('filter')(vm.critics, {
                        critic_id: criticId
                    })[0];
					selectedCritic.is_followed = "0";
					$loading.finish('commonLoader');   
				});
			} else if (isFollowed == "0") { // follow 
				$loading.start('commonLoader');
				PopularCriticsService.followLeaveCritic(sessionId, ftoken, criticId, "follow").then(function(response) {
					toastr.success(response.RESPONSE);
					var selectedCritic = $filter('filter')(vm.critics, {
                        critic_id: criticId
                    })[0];
					selectedCritic.is_followed = "1";
					//vm.critics[0].is_followed = "1";
					$loading.finish('commonLoader');   
				});
			}
		};
		
        vm.loadCritics();
    }

})();
