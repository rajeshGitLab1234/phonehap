 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        // $locationProvider.html5Mode(true);

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        $stateProvider

        .state('home', {
            url: '/',
            templateUrl: 'app/home/home.html',
            controller: 'HomeController',
            controllerAs: 'homeCtrl'
        });        

        $urlRouterProvider.otherwise(function($injector) {
          var $state = $injector.get('$state');
          $state.go('home', {}, { location: false });        
        });
    }
	
	angular
        .module('angularApp')
        .config(function(toastrConfig) {
		  angular.extend(toastrConfig, {
			autoDismiss: false,
			containerId: 'toast-container',
			maxOpened: 0,    
			newestOnTop: true,
			positionClass: 'toast-bottom-center',
			preventDuplicates: false,
			preventOpenDuplicates: false,
			target: 'body'
		  });
		});

})();