 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider
       
        .state('merchant-profile', {
            url: 'watch-it-on.html/:merchantname',
            templateUrl: 'app/merchantProfile/merchantProfile.html',
            controller: 'MerchantProfileController',
            controllerAs: 'MerchantProfileCtrl'
        });
        
        $urlRouterProvider.otherwise(function($injector, $location) {
          var $state = $injector.get('$state');          
            if($location.path() && $location.path().split('/') && $location.path().split('/').length > 2) {
                $state.go('merchant-profile', {merchantname: $location.path().split('/')[2]}, { location: false });
            } else {
                $state.go('merchant-profile', {}, { location: false });
            }
        });
    }
	
	angular
        .module('angularApp')
        .config(function(toastrConfig) {
		  angular.extend(toastrConfig, {
			positionClass: 'toast-bottom-center',
		  });
		});

})();