/**
 * @ngdoc controller
 * @name angularApp.controller:MerchantProfileController
 * @description
 * # MerchantProfileController
 * MerchantProfile Controller loads merchant profile data
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('MerchantProfileController', MerchantProfileController);

    MerchantProfileController.$inject = ['MerchantProfileService', 'StorageUtil', '$location', '$loading', '$stateParams', '$filter', 'UserAuthService', 'toastr'];

    function MerchantProfileController(MerchantProfileService, StorageUtil, $location, $loading, $stateParams, $filter, UserAuthService, toastr) {
    	var vm = this;     
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
        var merchantName = ($stateParams.merchantname) ? $stateParams.merchantname : angular.element('body').data('id');
		vm.currentLastMovie = 0;
		vm.lastLastMovie = -1;
		vm.isStillLoading = false;
		vm.reviewObj = {
            rating: 0,
            text: '',
            movie_id: ''
        };
		
		vm.isSeachBoxOpen = false;
        vm.advanceSearchObj = {
            movie_language: [],
            movie_genre: [],
            movie_decade: '',
            watch_available:''
        };
		vm.searchGenreList = [];
		vm.presetGenres = ['Romance','Action','Horror','Drama','Animation','Sci-Fi','Thriller','History','Comedy'];

        vm.loadMerchantProfileData = function() {
            
            $loading.start('commonLoader');
            MerchantProfileService.getMerchantProfile().then(function(merchantProfileData) {
                vm.merchantProfile = $filter('filter')(merchantProfileData, {
                                        value: merchantName
                                    })[0];  
                if(vm.merchantProfile) {
                    MerchantProfileService.getMerchantMovies(ftoken, vm.merchantProfile.value,0,vm.advanceSearchObj).then(function(movieResult) {
                        vm.movies = movieResult.RESPONSE;    
						vm.currentLastMovie = vm.movies.length;	
						// Get User Checkins
						MerchantProfileService.getCheckins(sessionId, ftoken).then(function(checkinResult) {
							for (var j = 0; j < checkinResult.RESPONSE.length; j++) {
								for (var i = 0; i < vm.movies.length; i++) {
									if (vm.movies[i].movie_id == checkinResult.RESPONSE[j].movie_id) {
										vm.movies[i].done_checkin = true;
										//console.log("match found");
										break;
									}
								}
							}
							$loading.finish('commonLoader');
						}, function(error){
							$loading.finish('commonLoader');
						});
                      
                    }, function(error){
                        $loading.finish('commonLoader');
                    });                     
                } else {
                    vm.movies = [];
                    $loading.finish('commonLoader');
                }

            }, function(error){
                $loading.finish('commonLoader');
            });                       
        };

		vm.loadMerchantMovies = function(startFrom) {
            $loading.start('commonLoader');
			// Get Actor Movies
			MerchantProfileService.getMerchantMovies(ftoken, vm.merchantProfile.value,startFrom,vm.advanceSearchObj).then(function(movieResult) {       
				if (startFrom > 0) { // next set of movies
					vm.movies = vm.movies.concat(movieResult.RESPONSE);  
				} else { // first set of movies
					vm.movies = movieResult.RESPONSE;  
				}
				     
				vm.currentLastMovie = vm.movies.length;	
				// Get User Checkins
				MerchantProfileService.getCheckins(sessionId, ftoken).then(function(checkinResult) {
					for (var j = 0; j < checkinResult.RESPONSE.length; j++) {
						for (var i = 0; i < vm.movies.length; i++) {
							if (vm.movies[i].movie_id == checkinResult.RESPONSE[j].movie_id) {
								vm.movies[i].done_checkin = true;
								//console.log("match found");
								break;
							}
						}
					}
					$loading.finish('commonLoader');
				}, function(error){
					$loading.finish('commonLoader');
				});
			}, function(error){
				$loading.finish('commonLoader');
			}); 
        };
		
		vm.loadMoreMovies = function() {
			if (vm.isStillLoading !== true ) { // only do next page if prev one has loaded 
				if (vm.currentLastMovie > vm.lastLastMovie) { // check if there were any results last time
					vm.lastLastMovie = vm.currentLastMovie;
					//console.log("loadMoreMovies:" + vm.currentLastMovie);
					vm.loadMerchantMovies(vm.currentLastMovie);
				}
			}
        };
		
		vm.setAdvanceSearchOption = function($event, searchType, searchVal) {
            if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length>0 && vm.advanceSearchObj[searchType].indexOf(searchVal) > -1) {
                vm.advanceSearchObj[searchType].splice(vm.advanceSearchObj[searchType].indexOf(searchVal), 1);
                angular.element($event.currentTarget).removeClass('activeIcon');
            } else if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length < 3) {
                vm.advanceSearchObj[searchType].push(searchVal);   
                angular.element($event.currentTarget).addClass('activeIcon'); 
            }            
        };

		vm.setAdvanceSearchOptionString = function($event, searchType, searchVal) {
			//console.log("setAdvanceSearchOptionString, searchType:" + searchType + ",searchVal:" + searchVal + ",event:" + $event.currentTarget);
			if (searchType == "movie_decade"){
				$('.RELEASEYEAR').removeClass('activeIconBackground');
			} else if (searchType == "watch_available"){
				$('.AVAILABLEWATCHto').removeClass('activeIconBackground');
			}
            vm.advanceSearchObj[searchType] = searchVal;    
			angular.element($event.currentTarget).addClass('activeIconBackground'); 
        };
		
        vm.Date = function(date) {
			if (angular.isDefined(date) && date != null) {
				return new Date(date.replace(/-/g, '/'));
			} else {
				return new Date(date);
			}
        };

        vm.range = function(n) {
            return new Array(parseInt(n));
        };

        vm.shareMovie = function() {
            $loading.start('commonLoader');
            MerchantProfileService.shareMovie(sessionId, ftoken, movieId).then(function(result) {
                $loading.finish('commonLoader');
                toastr.success(result.RESPONSE);             
            });
        };


        vm.loadMovieTrailer = function(movie_trailer) {
            vm.loadVideo = true;
            vm.movie_trailer = movie_trailer;

            angular.element('#introVideo').modal('show');
            angular.element('#introVideo').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
                vm.loadVideo = false;
            });
        }

        vm.routeToMovieDetail =  function(movie_id) {
            $state.go('movie-detail', {'id': movie_id}, {reload: true});
        };

        vm.getFormattedRating = function(rating) {
            if(rating && rating > 0) {
                return parseFloat(rating/2).toFixed(1);
            } else {
                return '--';
            }
        };

		vm.setRating = function(rating) {
            vm.reviewObj.rating = rating;
        };
		
		vm.showReviewBox = function(movie_id,done_checkin) {
			if (done_checkin !== true) {
				vm.reviewObj.movie_id = movie_id;
				vm.currentMovie = $filter('filter')(vm.movies, {
					movie_id: movie_id
				})[0];
				angular.element('#writeReview').modal('show');
			}
        }

        vm.postReviewForm = function() {
            if(vm.reviewObj.rating == 0) {
                alert("please select rating.");
            } else {
                MerchantProfileService.postReview(sessionId, ftoken, vm.reviewObj.movie_id, vm.reviewObj).then(function(result) {
                    vm.reviewObj = {
                        rating: 0,
                        text: '',
                        movie_id: ''
                    };
					vm.currentMovie.done_checkin = true;
                    angular.element('#writeReview').modal('hide');
                }, function(error){
                    $loading.finish('commonLoader');
                });    
            }
        };
		
		vm.searchGenre = function($event) {
			vm.searchValue = $event.currentTarget.value; 
			if (vm.searchValue.length > 0) {
				MerchantProfileService.searchGenre(vm.searchValue).then(function(result) {  
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchGenreList = result.RESPONSE;
                        } else {
                            vm.searchGenreList = [];
                        }                        
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });  
			}
		};
		
		vm.clearSearch = function(searchType) {
			if (searchType == "genre") {
				angular.element('#searchGenreBox').val('');
				vm.searchGenreList = [];
			}
		}
		
        vm.loadMerchantProfileData();
    }

})();
