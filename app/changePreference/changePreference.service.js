/**
 * @ngdoc service
 * @name angularApp.service:UserProfileService
 * @description
 * # UserProfileService
 * UserProfile Service for User controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('ChangePreferenceService', ChangePreferenceService);

    ChangePreferenceService.$inject = ['$http', '$q', 'CONSTANTS'];

    function ChangePreferenceService($http, $q, CONSTANTS) {

        var service = {            
            getRecommendationCriterion: getRecommendationCriterion,
            searchActor: searchActor,
            searchDirector: searchDirector,
			searchGenre: searchGenre,
			searchDecade: searchDecade,
            updateRecommendationCriterion: updateRecommendationCriterion,
			deleteRecommendations: deleteRecommendations,
			checkRecommendations: checkRecommendations
        };

        return service;

        function getRecommendationCriterion(ftoken, sessionId, userId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_RECOMMENDATION_CRITERION';
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getRecommendationCriterion", response.data); // same as  console.log(results[0]); line number 45
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function searchActor(searchTerm) {
            var url = CONSTANTS.API_URL+'&METHOD=SEARCH_ACTOR&start_from=0&max_results=5&search_term='+searchTerm;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchActor", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function searchDirector(searchTerm) {
			var url = CONSTANTS.API_URL+'&METHOD=SEARCH_DIRECTOR&start_from=0&max_results=5&search_term='+searchTerm;
            //var url = CONSTANTS.SITE_URL+'get_directors_json.php?search_term='+searchTerm;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchDirector", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		function searchGenre(searchTerm) {
			var url = CONSTANTS.API_URL+'&METHOD=SEARCH_RECOMMEND_GENRE&start_from=0&max_results=5&search_term='+searchTerm;
            //var url = CONSTANTS.SITE_URL+'get_genres_json.php?search_term='+searchTerm;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchGenre", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		function searchDecade(searchTerm) {
			var url = CONSTANTS.API_URL+'&METHOD=SEARCH_RECOMMEND_DECADE&start_from=0&max_results=5&search_term='+searchTerm;
            //var url = CONSTANTS.SITE_URL+'get_decades_json.php?search_term='+searchTerm;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchDecade", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
        function updateRecommendationCriterion(ftoken, sessionId, criterion_type, criterion_value) {
            var url = CONSTANTS.API_URL+'&METHOD=UPDATE_RECOMMENDATION_CRITERION&FTOKEN='+ftoken+'&SESSION='+sessionId+'&criterion_type='+criterion_type+'&criterion_value='+criterion_value;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("updateRecommendationCriterion", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		function deleteRecommendations(ftoken, sessionId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=DELETE_RECOMMENDATIONS';
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("deleteRecommendations", response.data); // same as  console.log(results[0]); line number 45
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		function checkRecommendations(ftoken, sessionId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_RECOMMENDATIONS';
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("deleteRecommendations", response.data); // same as  console.log(results[0]); line number 45
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
	}
})();