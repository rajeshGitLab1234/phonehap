/**
 * @ngdoc controller
 * @name angularApp.controller:ChangePreferenceController
 * @description
 * # ChangePreferenceController
 * Change Preference Controller loads change preference data
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('ChangePreferenceController', ChangePreferenceController);

    ChangePreferenceController.$inject = ['ChangePreferenceService', 'StorageUtil', '$location', '$loading', '$q', '$scope', '$state', '$interval', 'UserAuthService', 'toastr'];

    function ChangePreferenceController(ChangePreferenceService, StorageUtil, $location, $loading, $q, $scope, $state, $interval, UserAuthService, toastr) {
    	var vm = this;   
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
		if (!vm.isLogged) { // logged in
			// not logged in, redirect to main page
			window.location.href='index.html';
		}
        vm.searchValue = null;
        vm.advanceSearchObj = {
            movie_language: [],
            add_genre: [],
            add_actor: [],
            add_decade: [],
            add_director: []
        };
        vm.searchActorList = [];
        vm.searchDirectorList = [];
		vm.searchGenreList = [];
		vm.searchDecadeList = [];
		
        vm.loadChangePreferenceData = function() {
            
            $loading.start('commonLoader');
            var promises = [
                ChangePreferenceService.getRecommendationCriterion(ftoken, sessionId),
            ];
            //console.log(promises);

            $q.all(promises).then(function(results) {
                vm.recommendedData = results[0].RESPONSE[0];
                if(vm.recommendedData.genres && vm.recommendedData.genres.length>0) {
                    vm.advanceSearchObj.add_genre = vm.recommendedData.genres;
                }

                if(vm.recommendedData.language && vm.recommendedData.language.length>0) {
                    vm.advanceSearchObj.movie_language = vm.recommendedData.language;
                }
                
                if(vm.recommendedData.actors && vm.recommendedData.actors.length>0) {
                    vm.advanceSearchObj.add_actor = vm.recommendedData.actors;
                }
                if(vm.recommendedData.directors && vm.recommendedData.directors.length>0) {
                    vm.advanceSearchObj.add_director = vm.recommendedData.directors;
                }

                if(vm.recommendedData.decades && vm.recommendedData.decades.length>0) {
                    vm.advanceSearchObj.add_decade = vm.recommendedData.decades;
                }

                $loading.finish('commonLoader');                 
            }, function(error){
                $loading.finish('commonLoader');
            });                    
        };

        vm.setAdvanceSearchOption = function($event, searchType, searchVal) {
            if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length>0 && vm.advanceSearchObj[searchType].indexOf(searchVal) > -1) {
                vm.advanceSearchObj[searchType].splice(vm.advanceSearchObj[searchType].indexOf(searchVal), 1);
                angular.element($event.currentTarget).removeClass('activeIcon');
                vm.updateRecommendation(false, searchType, searchVal);
            } else if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length < 3) {
                vm.advanceSearchObj[searchType].push(searchVal);   
                angular.element($event.currentTarget).addClass('activeIcon');
                vm.updateRecommendation(true, searchType, searchVal); 
            }            
        };
        
        vm.updateActorList = function($event, actorName, actorId) {
			//console.log("updateActorList Name:" + actorName + ", ID:" + actorId);
            if(vm.advanceSearchObj.add_actor && vm.advanceSearchObj.add_actor.length>0 && vm.advanceSearchObj.add_actor.indexOf(actorName) > -1) {
                vm.advanceSearchObj.add_actor.splice(vm.advanceSearchObj.add_actor.indexOf(actorName), 1);
                angular.element($event.currentTarget).remove();
                vm.updateRecommendation(false, 'add_actor', actorName);
				toastr.success("Actor removed from your profile.");
            } else if(vm.advanceSearchObj.add_actor) {
                vm.advanceSearchObj.add_actor.push(actorName);   
                angular.element($event.currentTarget).addClass('activeIcon');
                vm.updateRecommendation(true, 'add_actor', actorId);
				toastr.success("Actor added to your profile.");
            }            
        };

        vm.updateDirectorList = function($event, directorName, directorId) {
            if(vm.advanceSearchObj.add_director && vm.advanceSearchObj.add_director.length>0 && vm.advanceSearchObj.add_director.indexOf(directorName) > -1) {
                vm.advanceSearchObj.add_director.splice(vm.advanceSearchObj.add_director.indexOf(directorName), 1);
                angular.element($event.currentTarget).remove();
                vm.updateRecommendation(false, 'add_director', directorName);
				toastr.success("Director removed from your profile.");
            } else if(vm.advanceSearchObj.add_director) {
                vm.advanceSearchObj.add_director.push(directorName);   
                angular.element($event.currentTarget).addClass('activeIcon');
                vm.updateRecommendation(true, 'add_director', directorId); 
				toastr.success("Director added to your profile.");
            }            
        };
		
		vm.updateDecadeList = function($event, decade) {
            if(vm.advanceSearchObj.add_decade && vm.advanceSearchObj.add_decade.length>0 && vm.advanceSearchObj.add_decade.indexOf(decade) > -1) {
                vm.advanceSearchObj.add_decade.splice(vm.advanceSearchObj.add_decade.indexOf(decade), 1);
                angular.element($event.currentTarget).remove();
                vm.updateRecommendation(false, 'add_decade', decade);
            } else if(vm.advanceSearchObj.add_decade) {
                vm.advanceSearchObj.add_decade.push(decade);   
                angular.element($event.currentTarget).addClass('activeIcon');
                vm.updateRecommendation(true, 'add_decade', decade); 
            }            
        };
		
		vm.updateGenreList = function($event, genre) {
            if(vm.advanceSearchObj.add_genre && vm.advanceSearchObj.add_genre.length>0 && vm.advanceSearchObj.add_genre.indexOf(genre) > -1) {
                vm.advanceSearchObj.add_genre.splice(vm.advanceSearchObj.add_genre.indexOf(genre), 1);
                angular.element($event.currentTarget).remove();
                vm.updateRecommendation(false, 'add_genre', genre);
            } else if(vm.advanceSearchObj.add_genre) {
                vm.advanceSearchObj.add_genre.push(genre);   
                angular.element($event.currentTarget).addClass('activeIcon');
                vm.updateRecommendation(true, 'add_genre', genre); 
            }            
        };

		vm.clearSearch = function(searchType) {
			if (searchType == "actor") {
				angular.element('#searchActorBox').val('');
				vm.searchActorList = [];
			} else if (searchType == "director") {
				angular.element('#searchDirectorBox').val('');
				vm.searchDirectorList = [];
			} else if (searchType == "decade") {
				angular.element('#searchDecadeBox').val('');
				vm.searchDecadeList = [];
			} else if (searchType == "genre") {
				angular.element('#searchGenreBox').val('');
				vm.searchGenreList = [];
			}
		}
        vm.loadSearchPage = function($event, type) {
            vm.searchValue = $event.currentTarget.value; 
			if (vm.searchValue.length > 0) {
				vm.loadStepSearchData(type);
			}
        };

        vm.loadStepSearchData = function(type) {
            $loading.start('commonLoader'); 
            switch(type) {
                case 'add_actor':
                    ChangePreferenceService.searchActor(vm.searchValue).then(function(result) {  
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchActorList = result.RESPONSE;
                        } else {
                            vm.searchActorList = [];
                        }                        
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });    
                    break;

                case 'add_director':
                    ChangePreferenceService.searchDirector(vm.searchValue).then(function(result) {  
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchDirectorList = result.RESPONSE;
                        } else {
                           vm.searchDirectorList = [];
                        }             
						//console.log("searchDirectorList:" + vm.searchDirectorList);
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });    
                    break;

				case 'add_decade':
                    ChangePreferenceService.searchDecade(vm.searchValue).then(function(result) {  
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchDecadeList = result.RESPONSE;
                        } else {
                            vm.searchDecadeList = [];
                        }                        
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });    
                    break;
					
				case 'add_genre':
                    ChangePreferenceService.searchGenre(vm.searchValue).then(function(result) {  
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchGenreList = result.RESPONSE;
                        } else {
                            vm.searchGenreList = [];
                        }                        
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });    
                    break;	
            }
            
        }
        vm.updateRecommendation = function(addStatus, searchType, searchVal) {
			//console.log("updateRecommendation:" + addStatus + " - " + searchType + " - " + searchVal);
			var promises = []; 
            if(addStatus) { //Add flow
                switch(searchType) {
                    case 'add_genre': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'add_genre', searchVal));
                        break;
                    case 'movie_language': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'add_language', searchVal));
                        break;
                    case 'add_decade': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'add_decade', searchVal));
                        break;
                    case 'add_actor': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'add_actor', searchVal));
                        break;
                    case 'add_director': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'add_director', searchVal));
                        break;
                }
            } else { //Remove Flow
                switch(searchType) {
                    case 'add_genre': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'remove_genre', searchVal));
                        break;
                    case 'add_decade': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'remove_decade', searchVal));
                        break;
                    case 'movie_language': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'remove_language', searchVal));
                        break;
                    case 'add_actor': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'remove_actor', searchVal));
                        break;
                    case 'add_director': 
                        promises.push(ChangePreferenceService.updateRecommendationCriterion(ftoken, sessionId, 'remove_director', searchVal));
                        break;
                }
            }
			$q.all(promises).then(function(results) {
				//console.log("loadChangePreferenceData after update");
				vm.loadChangePreferenceData();
			});
        };

		// Delete all recommendations and wait for new ones
		vm.refreshRecommendations = function() {
			ChangePreferenceService.deleteRecommendations(ftoken, sessionId).then(function(result) { 
				if(result.RESULT == 'SUCCESS') { // recommendations deleted
					var stop = $interval(function() {
						ChangePreferenceService.checkRecommendations(ftoken, sessionId).then(function(result) { 
							if (result.RESPONSE.length > 0) {
								$interval.cancel(stop);
								stop = undefined;
								window.location.href = 'discover-movies.html';
							}
						}, function(error){
							console.log("refreshRecommendations checkRecommendations error:" + error);
						});
					}, 15000);
				} else { // some error occured
					console.log("refreshRecommendations error:" + result.RESPONSE);
				}
			}, function(error){
				console.log("refreshRecommendations deleteRecommendations error:" + error);
			});
				
			angular.element('#waitingModal').modal();
		};
		
        vm.loadChangePreferenceData();
    }

})();
