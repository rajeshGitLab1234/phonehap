
angular
    .module('angularApp')
	.filter('html',function($sce){
	    return function(input){
	        return $sce.trustAsHtml(input);
	    }
	})
	.filter('trustYoutubeUrl',function($sce){
	    return function(input){
	        return $sce.trustAsResourceUrl("//www.youtube.com/embed/" + input);
	    }
	})
	.filter('range', function() {
	    return function(input, min, max) {
	        min = parseInt(min); //Make string input int
	        max = parseInt(max);
	        for (var i = min; i < max; i++)
	            input.push(i);
	        return input;
	    };
	});
