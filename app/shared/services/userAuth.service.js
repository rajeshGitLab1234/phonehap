/**
 * @ngdoc service
 * @name angularApp.service:UserAuthService
 * @description
 * # UserAuthService
 * User Authentication Service for all controllers
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .factory('UserAuthService', UserAuthService);

    UserAuthService.$inject = ['$http', '$q', 'CONSTANTS', 'StorageUtil'];

    function UserAuthService($http, $q, CONSTANTS, StorageUtil) {

		// Object to store user session
		var userObj = {
			name: '',
			email: '',
			sessionId: '',
			ftoken: '',
			loginType: ''
		}
		// Store User IP
		var originIP = StorageUtil.getLocal('originIP');
		if (!originIP) { 
			getUserIP(); // get IP one time
		}
		var isLogged = false;
		var refId = '';
		
		// Try getting stored user session object
		var storedUserObj = StorageUtil.getLocalObject('flickstreeUser');
		if (storedUserObj) {
			console.log("UserAuthService got userObj");
			userObj.sessionId = storedUserObj.sessionId;
			userObj.ftoken = storedUserObj.ftoken;
			userObj.loginType = storedUserObj.loginType;
			userObj.name = storedUserObj.name;
			userObj.email = storedUserObj.email;
			isLogged = true;
		} else { // no such object found
			console.log("UserAuthService set userObj");
		}
		
		// define service functions
        var service = {         
			doFBLogin: doFBLogin,
			getUserIP: getUserIP,
			getUserInfo: getUserInfo,
			statusChangeCallback: statusChangeCallback,			
			firstLogin: firstLogin,
			updateLogin: updateLogin,
			doGoogleLogin: doGoogleLogin,
			onGoogleSignIn: onGoogleSignIn,
			onGoogleSignOut: onGoogleSignOut,
			doAmazonLogin: doAmazonLogin,
			onSignOut: onSignOut
        };

        return service;

		// returns the user session object
		function getUserInfo() {
			return userObj;
		}
		
		// sets the external ip of user
		function getUserIP() {
			$.ajax({
				type: "GET",
				url: "https://loader.flickstree.com/get_ip.php"
			})
			.done(function( data ) {
				console.log("myexternalip data:" + data);
				if (data != '') {
					originIP = data;
					StorageUtil.setLocal('originIP',originIP);
				}
			}) 
			.fail(function() {
				//showNoInternet();
			}) 
			.always(function() {
			});
		}
		
		// opens facebook login window
		function doFBLogin() {
			var _self = this;
			FB.login(function(response){
				_self.statusChangeCallback(response);
			}, {scope: 'email,public_profile,user_friends'});
		}
		
		// facebook callback function
		function statusChangeCallback(response) {
			var _self = this;
			//console.log("statusChangeCallback response:" + JSON.stringify(response));
			if (response.status === 'connected') {
				// Logged into your app and Facebook.
				if (isLogged !== true) 
				{ // First Login 
					console.log('statusChangeCallback: connected but not logged in');
					_self.firstLogin(response.authResponse.accessToken); // Send Data to Server to store 
				} 
				else 
				{
					console.log('statusChangeCallback: connected and logged in');
					_self.updateLogin(response.authResponse.accessToken);
				}
			} 
			else if (response.status === 'not_authorized') 
			{
				console.log('statusChangeCallback: not_authorized');
				// The person is logged into Facebook, but not your app.
				StorageUtil.removeLocal('flickstreeUser');
				location.reload(true);
			} 
			else 
			{
				console.log('statusChangeCallback: not logged');
				// The person is not logged into Facebook, so we're not sure if
				// they are logged into this app or not.
				 $.ajax({
					url: "//www.flickstree.com/update_data_safe.php?doaction=fb_logout&id=" + userObj.ftoken + "&time=" + Date.now(),
					timeout: 60000,
					cache: false
				})
				.done(function( data ) {
					StorageUtil.removeLocal('flickstreeUser');
					location.reload(true);
				})
				.fail(function( data ) {
				})
				.always(function( data ) {
				});
			}
		}
		
		// save first login session to server and fetch ftoken
		function firstLogin(fbToken) {
			angular.element('#loggingOptions').hide();
			angular.element('#loginInProgress').show();
			//console.log('Welcome!  Fetching your information.... ');
			FB.api('/me', {fields: 'id,name,email,birthday,gender,hometown,timezone,locale'}, function(response) {
				//console.log("firstLogin /me response:" + JSON.stringify(response));
				console.log('Successful login for: ' + response.name);
				userObj.name = response.name;
				userObj.email = response.email;
				//var refId = getCookie('refId');
				$.ajax({
					type: "POST",
					url: "https://www.flickstree.com/fb_login_json.php",
					data: {"doaction": "fb_login", "user": response.id, "name": response.name, "email": response.email, 
							"username": response.username, "birthday": response.birthday, "gender": response.gender, 
							"hometown": response.hometown, "timezone": response.timezone, "locale": response.locale, 
							"fbtoken": fbToken, "refId": refId, "origin_ip": originIP}
				})
				.done(function( data ) {
					console.log("firstLogin: " + JSON.stringify(data));
					var dataObject = JSON && JSON.parse(data) || $.parseJSON(data);
					if (dataObject.ftoken && dataObject.ftoken != '') {
						console.log("setting vars");
						userObj.sessionId = dataObject.sessionId;
						userObj.ftoken = dataObject.ftoken;
						userObj.loginType = 'facebook';
						StorageUtil.setLocalObject('flickstreeUser',userObj);
						window.location.href='index.html';
					}
				}) 
				.fail(function() {
					angular.element('#loggingOptions').show();
					angular.element('#loginInProgress').hide();
				}) 
				.always(function() {
				});
			});
		}
		
		// update session to server
		function updateLogin(fbToken) {
			angular.element('#loggingOptions').hide();
			angular.element('#loginInProgress').show();
			console.log('Updating FB Token');
			$.ajax({
				type: "POST",
				url: "https://www.flickstree.com/fb_login_json.php",
				data: {"doaction": "fb_login", "ftoken": userObj.ftoken, "fbtoken": fbToken}
			})
			.done(function( data ) {
				userObj.sessionId = fbToken;
				userObj.loginType = 'facebook';
				StorageUtil.setLocalObject('flickstreeUser',userObj);
			}) 
			.fail(function() {
			}) 
			.always(function() {
				angular.element('#loggingOptions').show();
				angular.element('#loginInProgress').hide();
			});
		}
		
		// do Google Login
		function doGoogleLogin() {
			gapi.load('auth2', function() { //load in the auth2 api's, without it gapi.auth2 will be undefined
				gapi.auth2.init(
					{
						client_id: '675823471022-bde3fesvq89r02mj3buochcd6v87t2qs.apps.googleusercontent.com'
					}
				);
				var GoogleAuth  = gapi.auth2.getAuthInstance();//get's a GoogleAuth instance with your client-id, needs to be called after gapi.auth2.init
				GoogleAuth.signIn().then(function(response){//request to sign in
					console.log(response);
					onGoogleSignIn(response);
				});
			});
		}
		
		// Google Signin callback
		function onGoogleSignIn(googleUser) {
			angular.element('#loggingOptions').hide();
			angular.element('#loginInProgress').show();
			var profile = googleUser.getBasicProfile();
			//console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
			//console.log('Name: ' + profile.getName());
			//console.log('Image URL: ' + profile.getImageUrl());
			//console.log('Email: ' + profile.getEmail());
			userObj.name = profile.getName();
			userObj.email = profile.getEmail();
			var id_token = googleUser.getAuthResponse().id_token;
			//var refId = getCookie('refId');
			//console.log('id_token: ' + id_token);
			$.ajax({
					type: "POST",
					url: "https://www.flickstree.com/google_login_safe.php",
					data: {"idtoken": id_token, "refId": refId, "origin_ip": originIP}
			})
			.done(function( data ) {
				//console.log("data:" + data);
				var session_info = data.split(/,/);
				if (session_info.length == 2) {
					userObj.sessionId = session_info[1];
					userObj.ftoken = session_info[0];
					userObj.loginType = 'google';
					StorageUtil.setLocalObject('flickstreeUser',userObj);
					window.location.href='index.html';
				}
			}) 
			.fail(function() {
				//showNoInternet();
				angular.element('#loggingOptions').show();
				angular.element('#loginInProgress').hide();
			}) 
			.always(function() {
			});
		}
		
		// Google Signout
		function onGoogleSignOut() {
			gapi.load('auth2', function() { //load in the auth2 api's, without it gapi.auth2 will be undefined
				gapi.auth2.init(
					{
						client_id: '675823471022-bde3fesvq89r02mj3buochcd6v87t2qs.apps.googleusercontent.com'
					}
				);
				var GoogleAuth  = gapi.auth2.getAuthInstance();//get's a GoogleAuth instance with your client-id, needs to be called after gapi.auth2.init
				GoogleAuth.signOut().then(function(){//request to sign out
					console.log('Google User signed out.');
					StorageUtil.removeLocal('flickstreeUser');
					location.reload(true);
				});
			});
		}
		
		// opens Amazon Login
		function doAmazonLogin() {
			var options = { scope : 'profile' };
			amazon.Login.authorize(options, function(response) {
				if (response.error) {
					console.log("Login failed");
					return;
				}
				angular.element('#loggingOptions').hide();
				angular.element('#loginInProgress').show();
				// response.access_token
				$.ajax({
					type: "POST",
					url: "https://www.flickstree.com/amazon_login_json.php",
					data: {"access_token": response.access_token, "refId": refId, "origin_ip": originIP}
				})
				.done(function( data ) {
					console.log("doAmazonLogin: " + JSON.stringify(data));
					var dataObject = JSON && JSON.parse(data) || $.parseJSON(data);
					if (dataObject.ftoken && dataObject.ftoken != '') {
						console.log("setting vars");
						userObj.sessionId = dataObject.sessionId;
						userObj.ftoken = dataObject.ftoken;
						userObj.name = dataObject.name;
						userObj.email = dataObject.email;
						userObj.loginType = 'amazon';
						StorageUtil.setLocalObject('flickstreeUser',userObj);
						window.location.href='index.html';
					}
				}) 
				.fail(function() {
					angular.element('#loggingOptions').show();
					angular.element('#loginInProgress').hide();
				}) 
				.always(function() {
				});
			});
		}
		
		// generic sign out
		function onSignOut() {
			console.log('Google User signed out.');
			StorageUtil.removeLocal('flickstreeUser');
			location.reload(true);
		}		
    }
})();

