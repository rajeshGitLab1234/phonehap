/**
 * @ngdoc provider
 * @name angularApp.provider:CommonRouteProvider
 * @description
 * # CommonRouteProvider
 * CommonRouteProvider to provide common routes to all module
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .provider('CommonRouteProvider', CommonRouteProvider);

    CommonRouteProvider.$inject = [];

    function CommonRouteProvider() {
        var service = {            
            getextendedStates: getextendedStates            
        };

        return service;


.state('critics', {
            url: '/critics',
            // views: {
            //     'menuContent': {
                    templateUrl: 'app/critics/critics.html',
                    controller: 'CriticsController',
                    controllerAs: 'CriticsCtrl'
            //     }
            // }
        })
        .state('popuplar-critics', {
            url: '/popuplar-critics',
            // views: {
            //     'menuContent': {
                    templateUrl: 'app/popularCritics/popularCritics.html',
                    controller: 'PopularCriticsController',
                    controllerAs: 'PopularCriticsCtrl'
            //     }
            // }
        })
        .state('critics-profile-follow', {
            url: '/critics-profile-follow',
            // views: {
            //     'menuContent': {
                    templateUrl: 'app/criticsProfileFollow/criticsProfileFollow.html',
                    controller: 'CriticsProfileFollowController',
                    controllerAs: 'CriticsProfileFollowCtrl'
            //     }
            // }
        })
        .state('user-profile', {
            url: '/user-profile',
            // views: {
            //     'menuContent': {
                    templateUrl: 'app/userProfile/userProfile.html',
                    controller: 'UserProfileController',
                    controllerAs: 'UserProfileCtrl'
            //     }
            // }
        })
        .state('discover-movies', {
            url: '/discover-movies/:type',
            // views: {
            //     'menuContent': {
                    templateUrl: 'app/discoverMovies/discoverMovies.html',
                    controller: 'DiscoverMoviesController',
                    controllerAs: 'DiscoverMoviesCtrl'
            //     }
            // }
        });



        var menuItems = [
            {
                url: '/discover-movies/:type',
                templateUrl: 'app/discoverMovies/discoverMovies.html',
                controller: 'DiscoverMoviesController',
                controllerAs: 'DiscoverMoviesCtrl'
            }
            {
                label: 'Home',
                active: false,
                icon: 'svg-side-menu-home',
                url: '/app',
                state: 'app',
                templateUrl: 'tpl/home/dasboard.html',
                controller: 'HomeCtrl'
            }, {
                label: 'Charts',
                active: false,
                icon: 'svg-side-menu-chart-pie',
                url: '/charts',
                state: 'charts',
                templateUrl: 'tpl/charts/main-charts.html'
            }, {
                label: 'Settings',
                active: false,
                icon: 'svg-side-menu-settings',
                url: '/'
            }
        ];

        function getextendedStates($stateProvider) {
            menuItems.forEach(function (item) {
                if (item.state) {
                    let stateObj = {
                        url: item.url,
                        templateUrl: item.templateUrl,
                        controllerAs: 'vm'
                    };
                    if (item.controller) {
                        stateObj.controller = `${item.controller} as vm`;
                    }
                    $stateProvider.state(item.state, stateObj);
                } else {
                    item.state = defaultExistState;
                }
            });
        }

    }
})();


angular.module('app.side-menu', []);
    .provider('SideMenu', SideMenuProvider);

let 
const defaultExistState = menuItems[0].state;

function SideMenuProvider() {
    this.$get = function () {
        return {
            get items() {
                return menuItems;
            }
        };
    };
    this.extendStates = ExtendStates;
}

