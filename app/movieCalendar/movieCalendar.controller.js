/**
 * @ngdoc controller
 * @name angularApp.controller:MovieCalendarController
 * @description
 * # MovieCalendarController
 * Movie Calendar Controller to show the released movies list
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('MovieCalendarController', MovieCalendarController);

    MovieCalendarController.$inject = ['MovieCalendarService', 'StorageUtil', '$location', '$loading', '$sce', '$q', '$stateParams', 'UserAuthService'];

    function MovieCalendarController(MovieCalendarService, StorageUtil, $location, $loading, $sce, $q, $stateParams, UserAuthService) {
    	var vm = this;
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}        
        var movieId = angular.element('body').data('id');
        vm.loadVideo = false;
        vm.currentYear = new Date().getFullYear();
        
        vm.loadMovies = function() {
            
            $loading.start('commonLoader');

            MovieCalendarService.getMoviesData(sessionId, ftoken, vm.searchYear+'-'+vm.searchMonth).then(function(result) {
                vm.movies = result.RESPONSE;
				//console.log("Movies:" + vm.movies);
				for (var i in vm.movies) {
					if (vm.movies[i].audience_rating) {
						vm.movies[i].audience_rating = parseFloat(vm.movies[i].audience_rating/2).toFixed(1);
					}
				}					
                $loading.finish('commonLoader');
            }, function(error){
                $loading.finish('commonLoader');
            });            
        };

        vm.setSearchMonth = function(monthIndex, isSearch) {
            monthIndex = (monthIndex < 10) ? '0'+monthIndex: monthIndex;
            vm.searchMonth = monthIndex;
            if(isSearch) {
                vm.loadMovies();    
            }
        };

        vm.setSearchYear = function(yearIndex, isSearch) {
            vm.searchYear = yearIndex;
            if(isSearch) {
                vm.loadMovies();    
            }           
        };

        vm.loadMovieTrailer = function(movie_trailer) {
            vm.loadVideo = true;
            vm.movie_trailer = movie_trailer;

            angular.element('#introVideo').modal('show');
            angular.element('#introVideo').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
                vm.loadVideo = false;
            });
        };

        vm.getMonth = function(ind) {
            var dateObj = new Date().setMonth(ind);
            return dateObj;
        }
        vm.setSearchMonth(new Date().getMonth()+1);
        vm.setSearchYear(new Date().getFullYear());
        vm.loadMovies();
    }

})();
