/**
 * @ngdoc service
 * @name angularApp.service:MovieCalendarService
 * @description
 * # MovieCalendarService
 * Movie Calendar Service for MovieCalendar controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('MovieCalendarService', MovieCalendarService);

    MovieCalendarService.$inject = ['$http', '$q', 'CONSTANTS'];

    function MovieCalendarService($http, $q, CONSTANTS) {

        var service = {
            getMoviesData: getMoviesData
        };

        return service;

        function getMoviesData(sessionId, ftoken, year_month) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_FLICKSCALENDER&year_month='+year_month;
            var deferred = $q.defer();
			//console.log("URL:" + url);
            $http.get(url).then(function(response) {
                //console.log("getMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();
