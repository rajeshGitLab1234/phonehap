/**
 * @ngdoc service
 * @name angularApp.service:ActorProfileService
 * @description
 * # ActorProfileService
 * ActorProfile Service for ActorProfile controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('ActorProfileService', ActorProfileService);

    ActorProfileService.$inject = ['$http', '$q', 'CONSTANTS'];

    function ActorProfileService($http, $q, CONSTANTS) {

        var service = {            
            getActorProfile: getActorProfile,
            getActorMovies: getActorMovies,
            postReview: postReview,
            getCheckins: getCheckins,
			searchGenre: searchGenre
        };

        return service;

        function getActorProfile(actorName) {
            actorName = actorName.replace(/-/g, "+");
            var url = CONSTANTS.SITE_URL+'get_actors_json.php?term='+actorName;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getActorProfile", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getActorMovies(ftoken,actorId,startFrom,advanceSearchObj) {
			startFrom = (startFrom) ? startFrom : "0";
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&METHOD=GET_ACTOR_MOVIES&critics_recommended_only=N&order_by=weighed_rating+DESC&actor_id='+actorId + '&start_from='+startFrom;
			//console.log("URL:" + url);
            var deferred = $q.defer();
			
			if (advanceSearchObj) {
				if(advanceSearchObj.movie_genre && advanceSearchObj.movie_genre.length >0) {
					url += "&movie_genre=" + advanceSearchObj.movie_genre.join('@');
				}

				if(advanceSearchObj.movie_language && advanceSearchObj.movie_language.length >0) {
					url += "&movie_language=" + advanceSearchObj.movie_language.join('@');
				}

				if(advanceSearchObj.movie_decade && advanceSearchObj.movie_decade != '') {
					if(advanceSearchObj.movie_decade == '1950-2000') {
						url += "&movie_decade=" + '1950@1960@1970@1980@1990';
					} else if(advanceSearchObj.movie_decade == '2000-2010') {
						url += "&movie_decade=" + '2000';
					} else if(advanceSearchObj.movie_decade == '2010-2016') {
						//var currentYear = new Date().getFullYear();
						url += "&movie_decade=" + '2010';
					}                
				}

				if(advanceSearchObj.watch_available && advanceSearchObj.watch_available != "") {
					url += "&watch_available=" + advanceSearchObj.watch_available;
				}
			}
			
            $http.get(url).then(function(response) {
                //console.log("getActorMovies", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		// Post Review or Add Checkin
		function postReview(sessionId, ftoken, movieId, reviewObj) {
			var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId;
			if (reviewObj.text.length > 0) {
				var reviewText = reviewObj.text.replace(/\s/g, "+");
				url += '&METHOD=ADD_USER_REVIEW&movie_id='+movieId+'&review_title='+reviewText+'&review_text='+reviewText+'&rating='+reviewObj.rating;
			} else {
				url += '&METHOD=ADD_CHECKIN&movie_id='+movieId+'&checkin_rating='+reviewObj.rating;
			}
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("postReview", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise; 
        }
		
		function getCheckins(sessionId, ftoken) {
            //var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_USER_CHECKIN&max_results=1000';
			//console.log("getCheckins URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCheckins", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		function searchGenre(searchTerm) {
			var url = CONSTANTS.API_URL+'&METHOD=SEARCH_RECOMMEND_GENRE&start_from=0&max_results=5&search_term='+searchTerm;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchGenre", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();