/**
 * @ngdoc controller
 * @name angularApp.controller:HomeController
 * @description
 * # HomeController
 * Home Controller loads home page data
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['HomeService', 'StorageUtil', '$window', '$loading', '$sce', '$q', '$timeout', '$filter', '$state', 'CONSTANTS','UserAuthService', 'toastr'];

    function HomeController(HomeService, StorageUtil, $window, $loading, $sce, $q, $timeout, $filter, $state, CONSTANTS, UserAuthService, toastr) {
    	var vm = this;
		
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		vm.name = '';
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.name = userObj.name;
				vm.isLogged = true;
			}
		}
		
        vm.currentMovieObj = {
            movie_name: '',
            audience_rating: '',
            critics_rating: '',
            movie_release_date: '',
            month: '',
            movie_id: ''
        };

        vm.advanceSearchObj = {
            movie_language: ['English'],
            movie_genre: [],
            movie_cast: [],
            movie_decade: '',
            movie_release_date_start: '',
            movie_release_date_end: '',
            watch_available:''
        };

        vm.loadHomePageData = function() {
            
            $loading.start('commonLoader');

            var promises = [
                HomeService.getRunningNowMoviesData(sessionId, ftoken),
                HomeService.getUpcomingMoviesData(sessionId, ftoken),
                HomeService.getCriticsData(sessionId, ftoken),
                HomeService.getRecommendedMoviesData(sessionId, ftoken),
                HomeService.getCriticsRecommendedMoviesData(sessionId, ftoken),
                HomeService.getMiscData(sessionId, ftoken)
            ];
                
            $q.all(promises).then(function(result) {
                
                var runningMovieList = result[0].RESPONSE || [];
                var upcomingMovieList = result[1].RESPONSE || [];
                vm.criticList = result[2].RESPONSE;
                vm.miscData = result[5].RESPONSE;
                
                if(result[3].RESPONSE && result[3].RESPONSE.length > 0) {
                    vm.recommendedMovie = $filter('filter')(result[3].RESPONSE, {
                        movie_poster: '!!'
                    })[0];
                } else { 
					angular.element('#welcomeFirst').modal({backdrop: "static"});
				}
                
                if(result[4].RESPONSE && result[4].RESPONSE.length > 0) {
                    vm.criticsRecommendedMovie = $filter('filter')(result[4].RESPONSE, {
                        movie_poster: '!!'
                    })[0];
                } 

                runningMovieList.sort(function(a, b) {
                    var dateA = new Date(a.movie_release_date), dateB = new Date(b.movie_release_date);
                    return (dateA-dateB);
                });

				for (var i in runningMovieList) {
					runningMovieList[i].audience_rating = (runningMovieList[i].audience_rating) ? parseFloat(runningMovieList[i].audience_rating/2).toFixed(1) : '--';
					runningMovieList[i].critics_rating = (runningMovieList[i].critics_rating) ? parseFloat(runningMovieList[i].critics_rating/2).toFixed(1) : '--';
					if(runningMovieList[i].movie_release_date) {
						runningMovieList[i].movie_release_date = new Date(runningMovieList[i].movie_release_date);
					}
					if(runningMovieList[i].movie_genres){
						var genre = runningMovieList[i].movie_genres;
						runningMovieList[i].movie_genres =  (genre) ? genre.splice(0, 3).join(', ') : "";
					}
				}
					
                upcomingMovieList.sort(function(a, b) {
                    var dateA = new Date(a.movie_release_date), dateB = new Date(b.movie_release_date);
                    return (dateA-dateB);
                });

				for (var i in upcomingMovieList) {
					upcomingMovieList[i].audience_rating = (upcomingMovieList[i].audience_rating) ? parseFloat(upcomingMovieList[i].audience_rating/2).toFixed(1) : '--';
					upcomingMovieList[i].critics_rating = (upcomingMovieList[i].critics_rating) ? parseFloat(upcomingMovieList[i].critics_rating/2).toFixed(1) : '--';
					if(upcomingMovieList[i].movie_release_date) {
						upcomingMovieList[i].movie_release_date = new Date(upcomingMovieList[i].movie_release_date);
					}
					if(upcomingMovieList[i].movie_genres){
						var genre = upcomingMovieList[i].movie_genres;
						upcomingMovieList[i].movie_genres =  (genre) ? genre.splice(0, 3).join(', ') : "";
					}
				}
				
				// Combine Running with Upcoming movies
                vm.movieList = runningMovieList.concat(upcomingMovieList);

                vm.currentMovieObj = vm.movieList[1];
                vm.movie_trailer = vm.currentMovieObj.movie_trailer; 
				vm.currentMovieObj.audience_rating = vm.movieList[1].audience_rating;
				vm.currentMovieObj.critics_rating = vm.movieList[1].critics_rating;
				
				if(vm.movieList[1].movie_release_date) {
					vm.currentMovieObj.movie_release_date = vm.movieList[1].movie_release_date;
				}
				
				if (vm.movieList[1].movie_genres) {
					vm.currentMovieObj.movie_genres = vm.movieList[1].movie_genres;
				}

				// Add Swiper Control for combined list
                $timeout(function() {
                    var that = this;
                    var swiper = new Swiper('.swiper-container', {                            
                        slidesPerView: 3,
                        pagination: '.swiper-pagination',
                        centeredSlides:true,
                        initialSlide: 1,
                        spaceBetween: 10,
                        onSlideChangeEnd: function (swiperObj) {
                            var slideId = $(swiperObj.slides[swiperObj.activeIndex]).data('id');
                            var movieObj = $filter('filter')(that.movieList, {
                                movie_id: slideId
                            });
                            if(movieObj && movieObj[0]) {
                                $timeout(function(movieObj) {
                                    this.currentMovieObj = movieObj[0];
                                    this.movie_trailer = this.currentMovieObj.movie_trailer;
									this.currentMovieObj.audience_rating = movieObj[0].audience_rating;
									this.currentMovieObj.critics_rating = movieObj[0].critics_rating;
                                    if(movieObj[0].movie_release_date) {
										this.currentMovieObj.movie_release_date = movieObj[0].movie_release_date;
                                    }
                                    if(movieObj[0].movie_genres){
										vm.currentMovieObj.movie_genres = movieObj[0].movie_genres;
                                    }
                                }.bind(that, movieObj), 0);
                            }                            
                        },
                        onClick: function(swiper, event) {
                            that.loadMovieTrailer();
                        }
                    });
                }.bind(vm), 0);
                
                $('#myCarousel').carousel({
                  interval: 5000,
                  cycle: true
                }); 
                angular.element('.navbar-collapse').on('show.bs.collapse', function () {
                    angular.element('#logoText').css('color', '#ffffff');
                });
                angular.element('.navbar-collapse').on('hide.bs.collapse', function () {
                    angular.element('#logoText').css('color', '#cb202e');
                });
                $loading.finish('commonLoader'); 
            }, function(error){
                $loading.finish('commonLoader');
            });            
        };

		// Movie Detail Route
        vm.routeToMovieDetail =  function(seo_link,movie_id) {
			if (seo_link) {
				$window.location.href = CONSTANTS.SITE_URL + 'mobile' + seo_link;
			} else {
				$window.location.href = 'movie-details.html' + '/' + movie_id;
			}
        };

		// Movie Discovery Route
        vm.routeToDiscoverMovie = function(pageType) {
            if(pageType == 'search') {
                StorageUtil.setLocalObject('searchObj', vm.advanceSearchObj);
                window.location.href = 'movie-options.html';
            } else if(pageType == 'recommended') {
                window.location.href = 'discover-movies.html';
            } else if(pageType == 'criticsRecommended') {
                window.location.href = 'critic-recommendation.html';
            }
        };

		// Popular Critics Route
        vm.routeToPopularCritics = function() {
            $window.location.href = 'popular-critics.html';
        };

		// Process Advance Search
        vm.setAdvanceSearchOption = function($event, searchType, searchVal) {
            if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length>0 && vm.advanceSearchObj[searchType].indexOf(searchVal) > -1) {
                vm.advanceSearchObj[searchType].splice(vm.advanceSearchObj[searchType].indexOf(searchVal), 1);
                angular.element($event.currentTarget).removeClass('activeIcon');
            } else if(vm.advanceSearchObj[searchType] && vm.advanceSearchObj[searchType].length < 3) {
                vm.advanceSearchObj[searchType].push(searchVal);   
                angular.element($event.currentTarget).addClass('activeIcon'); 
            }            
        };

        vm.setAdvanceSearchOptionString = function($event, searchType, searchVal) {
			if (searchType == "movie_decade"){
				$('.RELEASEYEAR').removeClass('activeIconBackground');
			} else if (searchType == "watch_available"){
				$('.AVAILABLEWATCHto').removeClass('activeIconBackground');
			}
            vm.advanceSearchObj[searchType] = searchVal;    
			angular.element($event.currentTarget).addClass('activeIconBackground'); 
        };

        vm.Date = function(date) {
			if (angular.isDefined(date) && date != null) {
				return new Date(date.replace(/-/g, '/'));
			} else {
				return new Date(date);
			}
        };

        vm.launchWatchUrl = function(url) {
            window.open(url, '_blank');
        };

        vm.range = function(n) {
            return new Array(parseInt(n));
        };

        vm.addToWatchList = function() {
            $loading.start('commonLoader');
            HomeService.addToWatchList(sessionId, ftoken, movieId).then(function(result) {
                $loading.finish('commonLoader');
				toastr.success(response.RESPONSE);
            });
        };

        vm.shareMovie = function() {
            $loading.start('commonLoader');
            HomeService.shareMovie(sessionId, ftoken, movieId).then(function(result) {
                $loading.finish('commonLoader');
				toastr.success(response.RESPONSE);				
            });
        };

        vm.postReviewForm = function() {
            if(vm.reviewObj.rating == 0) {
                alert("please select rating.");
            } else {
                $loading.start('commonLoader');
                HomeService.postReview(sessionId, ftoken, movieId, vm.reviewObj).then(function(result) {
                    $loading.finish('commonLoader');
                    angular.element('#writeReview').modal('hide');
					toastr.success(response.RESPONSE);
                });    
            }
        };

        vm.setRating = function(rating) {
            vm.reviewObj.rating = rating;
        };

        vm.trustSrcurl = function(data) {            
            return $sce.trustAsResourceUrl("//www.youtube.com/embed/" + data);
        };

        vm.toggleDescription = function() {
            angular.element('#movie_desc').toggleClass('SYNOPSIS SYNOPSIS-auto', 'slow', "easeOutSine");
        };

        vm.skipThis = function() {
            angular.element('html, body').animate({
                scrollTop: angular.element("#discoverSection").offset().top
            }, 1000);
        };


        vm.loadMovieTrailer = function() {
            angular.element('#introVideoHome').modal('show');
            angular.element('#introVideoHome').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
            });
        };

		vm.followLeaveCritic = function($event,criticId,isFollowed) {
			if (isFollowed == "1") { // already followed, leave 
				$loading.start('commonLoader');
				HomeService.followLeaveCritic(sessionId, ftoken, criticId, "leave").then(function(response) {
					toastr.success(response.RESPONSE);
					$filter('filter')(vm.criticList, {
                                critic_id: criticId
                            })[0].is_followed = "0";
					$loading.finish('commonLoader');   
				});
			} else if (isFollowed == "0") { // follow 
				$loading.start('commonLoader');
				HomeService.followLeaveCritic(sessionId, ftoken, criticId, "follow").then(function(response) {
					toastr.success(response.RESPONSE);
					$filter('filter')(vm.criticList, {
                                critic_id: criticId
                            })[0].is_followed = "1";
					$loading.finish('commonLoader');   
				});
			}
		};
		
		vm.prevSlide = function() {
			$('#myCarousel').carousel('prev');
		};
		
		vm.nextSlide = function() {
			$('#myCarousel').carousel('next');
		}
		
        vm.loadHomePageData();
    }

})();
