/**
 * @ngdoc service
 * @name angularApp.service:HomeService
 * @description
 * # HomeService
 * Home Service for home controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('HomeService', HomeService);

    HomeService.$inject = ['$http', '$q', 'CONSTANTS'];

    function HomeService($http, $q, CONSTANTS) {

        var service = {
            getCriticsData: getCriticsData,
            getUpcomingMoviesData: getUpcomingMoviesData,
            getRunningNowMoviesData: getRunningNowMoviesData,
            getUserReviewData: getUserReviewData,
            addToWatchList: addToWatchList,
            postReview: postReview,
            getRecommendedMoviesData: getRecommendedMoviesData,
            getCriticsRecommendedMoviesData: getCriticsRecommendedMoviesData,
            getMiscData: getMiscData,
			followLeaveCritic: followLeaveCritic
        };

        return service;

        function getUpcomingMoviesData(sessionId, ftoken) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_UPCOMING_MOVIES';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUpcomingMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getRunningNowMoviesData(sessionId, ftoken) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_RUNNING_NOW_MOVIES';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getRunningNowMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
        
        function getUserReviewData(sessionId, ftoken, movieId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_MOVIE_USERS_REVIEWS&movie_id='+movieId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUserReviewData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }        

        function getCriticsData(sessionId, ftoken,language) {
			if (!language) {
				language = "Hindi";
			}
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&METHOD=GET_CRITICS_BY_REVIEWS&language='+language;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCriticsData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function addToWatchList(sessionId, ftoken, movieId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=ADD_WATCHLIST&movie_id='+movieId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("addToWatchList", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		// Post Review or Add Checkin
		function postReview(sessionId, ftoken, movieId, reviewObj) {
			var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId;
			if (reviewObj.text.length > 0) {
				var reviewText = reviewObj.text.replace(/\s/g, "+");
				url += '&METHOD=ADD_USER_REVIEW&movie_id='+movieId+'&review_title='+reviewText+'&review_text='+reviewText+'&rating='+reviewObj.rating;
			} else {
				url += '&METHOD=ADD_CHECKIN&movie_id='+movieId+'&checkin_rating='+reviewObj.rating;
			}
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("postReview", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise; 
        }
		
        function getRecommendedMoviesData(sessionId, ftoken) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_RECOMMENDATIONS';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getRecommendedMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getCriticsRecommendedMoviesData(sessionId, ftoken) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_CRITICS_RECOMMENDED_MOVIES_ADVANCED';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCriticsRecommendedMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getMiscData(sessionId, ftoken) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_MISC_DATA';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getMiscData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
		
		function followLeaveCritic(sessionId, ftoken, criticId, actionType) {
			var url;
			if (actionType == "follow") {
				url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=FOLLOW_CRITIC&critic_id='+criticId;
			} else if (actionType == "leave"){
				url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=LEAVE_CRITIC&critic_id='+criticId;
			}
            //var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=FOLLOW_CRITIC&critic_id='+criticId;
			//console.log("URL:" + url);
			
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("followCritic", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();
