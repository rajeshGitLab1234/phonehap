 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider
        
        .state('onboarding', {
            url: '/',
            templateUrl: 'app/onBoarding/onBoarding.html',
            controller: 'OnBoardingController',
            controllerAs: 'onBoardingCtrl'           
        });
        
        $urlRouterProvider.otherwise(function($injector) {
          var $state = $injector.get('$state');          
            $state.go('onboarding', {}, { location: false });
        });
    }

})();