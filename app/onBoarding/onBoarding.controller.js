/**
 * @ngdoc controller
 * @name angularApp.controller:OnBoardingController
 * @description
 * # OnBoardingController
 * OnBoarding Controller to collect the user preference
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('OnBoardingController', OnBoardingController);

    OnBoardingController.$inject = ['OnBoardingService', 'StorageUtil', '$window', '$loading', '$sce', '$q', '$interval', '$filter', '$state', 'UserAuthService'];

    function OnBoardingController(OnBoardingService, StorageUtil, $window, $loading, $sce, $q, $interval, $filter, $state, UserAuthService) {
    	var vm = this;
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		vm.name = '';
		vm.email = '';
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
				vm.name = userObj.name;
				vm.email = userObj.email;
			}
		}
        
		if (!vm.isLogged) { // logged in
			// not logged in, redirect to main page
			window.location.href='index.html';
		}
				
        var promises = [];
        vm.actorList = [];
        vm.criticList = [];
        vm.movieList = [];

        vm.searchActorList = [];
        vm.searchCriticList = [];
        vm.searchMovieList = [];
        
        vm.recommendedRating = 0;
        vm.currentPage = 0;
        vm.boardingData = {
            add_language: [],
            add_genre: [],
            add_actor: [],
            follow_critic: [],
        };
        vm.selectedMovieListObj = {};
        vm.searchValue = null;
        vm.currentStep = 1;
        vm.currentSearchPage = 1;
		vm.isStillLoading = false;
		vm.currentLastActor = 0;
		vm.lastLastActor = -1;
		vm.currentLastCritic = 0;
		vm.lastLastCritic = -1;
		vm.currentLastMovie = 0;
		vm.lastLastMovie = -1;
		
        vm.nextButton = false;
        vm.skipButton = 0;
        vm.prevButton = false;
		
		vm.savedLanguages = vm.savedGenres = "";

		// Load Screen data based on Step no
        vm.loadStep = function() {
            switch(vm.currentStep) {
                case 1:  //Language Screen            
                    break;
                case 2:   //Genre Screen

                    break;
                case 3:   //Actor List
                    angular.element('#searchBox').attr('placeholder', 'Search Actors...');
					vm.isStillLoading = true;
                    $loading.start('commonLoader'); 
					var lang = vm.boardingData.add_language.join("@");
                    OnBoardingService.getRecommendedActor(ftoken, sessionId, vm.currentPage,lang).then(function(result) { 
                        vm.actorList = vm.actorList.concat(result.RESPONSE);
						vm.currentLastActor = vm.actorList.length;
                        $loading.finish('commonLoader'); 
						vm.isStillLoading = false;
                    }, function(error){
                        $loading.finish('commonLoader');
						vm.isStillLoading = false;
                    });               
                    break;

                case 4:   //Follow critic
                    angular.element('#searchBox').attr('placeholder', 'Search Critic...');
					vm.isStillLoading = true;
                    $loading.start('commonLoader'); 
                    OnBoardingService.getRecommendedCritics(ftoken, sessionId, vm.currentPage).then(function(result) { 
                        vm.criticList = vm.criticList.concat(result.RESPONSE);
						vm.currentLastCritic = vm.criticList.length;
                        $loading.finish('commonLoader'); 
						vm.isStillLoading = false;
                    }, function(error){
                        $loading.finish('commonLoader');
						vm.isStillLoading = false;
                    });    
                      
                    break;

                case 5:   //Movie listing
                    angular.element('#searchBox').attr('placeholder', 'Search Film...');
					vm.isStillLoading = true;
                    $loading.start('commonLoader'); 
					//console.log("now saveBoardingInformation");
                    vm.saveBoardingInformation().then(function() { 
						//console.log("now getFlicktreeRecommendedMovies with page:" + vm.currentPage);
                        OnBoardingService.getFlicktreeRecommendedMovies(ftoken, sessionId, vm.movieList.length,vm.savedLanguages,vm.savedGenres).then(function(result) { 
							if (result.RESPONSE.length <= 0) { // no further results
								vm.savedGenres = ''; // empty genres now
								OnBoardingService.getFlicktreeRecommendedMovies(ftoken, sessionId, vm.movieList.length,vm.savedLanguages,vm.savedGenres).then(function(result) {
									vm.addUniqueMovies(result.RESPONSE);
									$loading.finish('commonLoader');
									vm.isStillLoading = false;
								}, function(error){
									$loading.finish('commonLoader');
									vm.isStillLoading = false;
								}); 
							} else {
								vm.addUniqueMovies(result.RESPONSE);
								//vm.movieList = vm.movieList.concat(result.RESPONSE);
								//vm.currentLastMovie = vm.movieList.length;
								$loading.finish('commonLoader'); 
								vm.isStillLoading = false;
							}                            
                        }, function(error){
                            $loading.finish('commonLoader');
							vm.isStillLoading = false;
                        }); 
                    }, function(error){
                        $loading.finish('commonLoader');
						vm.isStillLoading = false;
                    });                          
                    break;
            }
            vm.checkForNavigationVisibility();
        };

		// Check for duplicate movies and add
		vm.addUniqueMovies = function(newMovies) {
			for (var i = 0; i < newMovies.length; i++) {
				var checkMovieId = newMovies[i].movie_id;
				var testDup = $filter('filter')(vm.movieList, {
					movie_id: checkMovieId
				});
				if (testDup.length <= 0) { // Unique 
					vm.movieList.push(newMovies[i]);
				}
			}
			vm.currentLastMovie = vm.movieList.length;
		};
		// Next Nav Button
        vm.loadNextStep = function() {
            if (vm.currentStep < 5) {
                vm.currentStep++;
                vm.currentPage = 0;
                vm.currentSearchPage = 0;
                vm.searchValue = null;
                vm.loadStep();
            }  else if (vm.currentStep == 5) {
                //$state.go('discover-movies', {'type': 'recommended'}, {reload: true});
				var stop = $interval(function() {
					OnBoardingService.checkRecommendations(ftoken, sessionId).then(function(result) { 
						if (result.RESPONSE.length > 0) {
							$interval.cancel(stop);
							stop = undefined;
							window.location.href = 'discover-movies.html';
						}
                    }, function(error){
						
                    });
					
				}, 15000);
				angular.element('#waitingModal').modal();
            }
        };

		// Prev Nav button 
        vm.loadPreviousStep = function() {
            if(vm.currentStep > 1) {
                vm.currentStep--;
                vm.currentPage = 0;
                vm.currentSearchPage = 0;
                vm.searchValue = null;
                vm.loadStep();    
            }            
        };
        
		// Next Page Loader
        vm.loadNextPage = function() {
			//console.log("loadNextPage currentStep:" + vm.currentStep);
			if (vm.isStillLoading !== true) {
				if (vm.currentStep == 3) { // actor
					//console.log("loadNextPage currentStep:" + vm.currentStep);
					if (vm.currentLastActor > vm.lastLastActor) { // check if there were any results last time
						vm.lastLastActor = vm.currentLastActor;
						vm.currentPage++;
						vm.loadStep();
					}
				}
				else if (vm.currentStep == 4) { // critic
					//console.log("loadNextPage currentStep:" + vm.currentStep);
					if (vm.currentLastCritic > vm.lastLastCritic) { // check if there were any results last time
						vm.lastLastCritic = vm.currentLastCritic;
						vm.currentPage++;
						vm.loadStep();
					}
				}
				else if (vm.currentStep == 5) { // movie
					//console.log("loadNextPage currentStep:" + vm.currentStep);
					if (vm.currentLastMovie > vm.lastLastMovie) { // check if there were any results last time
						vm.lastLastMovie = vm.currentLastMovie;
						vm.currentPage++;
						vm.loadStep();
					}
				}
			}
        };

		// Search Box Change trigger
        vm.loadSearchPage = function($event) {
            vm.searchValue = $event.currentTarget.value; 
			if (vm.searchValue && vm.searchValue.length > 0) {
				vm.loadStepSearchData();
			}
        };

		// Next Search page
        vm.loadNextSearchPage = function() {
            vm.currentSearchPage++;
            if(vm.searchValue) {
                vm.loadStepSearchData();    
            }            
        };

		// External Search
        vm.loadStepSearchData = function() {
            switch(vm.currentStep) {
                case 3:   //Actor List
                    $loading.start('commonLoader'); 
                    OnBoardingService.searchActor(ftoken, sessionId, vm.searchValue).then(function(result) {  
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchActorList = result.RESPONSE;
                        } else {
                            vm.searchActorList = [];
                        }                        
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });               
                    break;

                case 4:   //Follow critic
                    $loading.start('commonLoader'); 
                    OnBoardingService.searchCritic(ftoken, sessionId, vm.searchValue).then(function(result) { 
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchCriticList = result.RESPONSE;
                        } else {
                            vm.searchCriticList = [];
                        }                        
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    });    
                      
                    break;
                case 5:   //Movie listing
                    $loading.start('commonLoader'); 
                    OnBoardingService.searchMovies(ftoken, sessionId, vm.searchValue).then(function(result) {
                        if(result.RESULT == 'SUCCESS') {
                            vm.searchMovieList = result.RESPONSE;
                        } else {
                            vm.searchMovieList = [];
                        }
                        $loading.finish('commonLoader'); 
                    }, function(error){
                        $loading.finish('commonLoader');
                    }); 
                                              
                    break;
            }
        };

		// Save Recommendation Criterion in memory
        vm.setBoardingData = function($event, key, value){
            if(vm.boardingData[key] && vm.boardingData[key].length>0 && vm.boardingData[key].indexOf(value) > -1) {
                vm.boardingData[key].splice(vm.boardingData[key].indexOf(value), 1);
                angular.element($event.currentTarget).removeClass('activeIcon');
            } else if(vm.boardingData[key] && vm.boardingData[key].length < 3) {
                vm.boardingData[key].push(value);   
                angular.element($event.currentTarget).addClass('activeIcon'); 
				
            }
            vm.checkForNavigationVisibility(); 
        };

		// Shift Search and Selected Data to main window
		vm.addSearchData = function(dataType,dataObject) {
			if (dataType == "actor") {
				var dups = $filter('filter')(vm.actorList, {
					actor_id: dataObject.actor_id
				})[0];
				if (!dups) {
					vm.actorList.unshift(dataObject);
				}
				vm.searchActorList = [];
			} else if (dataType == "critic") {
				var dups = $filter('filter')(vm.criticList, {
					critic_id: dataObject.critic_id
				})[0];
				if (!dups) {
					vm.criticList.unshift(dataObject);
				}
				vm.searchCriticList = [];
			} else if (dataType == "movie") {
				var dups = $filter('filter')(vm.movieList, {
					movie_id: dataObject.movie_id
				})[0];
				if (!dups) {
					vm.movieList.unshift(dataObject);
				}
				vm.searchMovieList = [];
			}
			angular.element('#myBoardingsearchBox').val('');
			angular.element('#myBoardingModal').modal('hide');
		};
		
		// Save Recommendation Criterion to Server
        vm.saveBoardingInformation = function() {
            var deferred = $q.defer();

            for(var key in vm.boardingData) {
                if(vm.boardingData[key] && vm.boardingData[key].length > 0) {
                    if(key == 'follow_critic') {
                        vm.boardingData[key].forEach(function(element) {
                            promises.push(OnBoardingService.followCritic(ftoken, sessionId, element));            
                        });
                    } else if(key != 'movie_id'){
                        vm.boardingData[key].forEach(function(element) {
                            promises.push(OnBoardingService.updateRecommendationCriterion(ftoken, sessionId, key, element));            
                        });
                    }                    
                }
            }

            if(promises && promises.length > 0) {
                $q.all(promises).then(function(result) {                
                    deferred.resolve(); 
					if (angular.isDefined(vm.boardingData.add_language) && vm.boardingData.add_language.length > 0) {
						vm.savedLanguages = vm.boardingData.add_language.join("@");
					}
					if (angular.isDefined(vm.boardingData.add_genre) && vm.boardingData.add_genre.length > 0) {
						vm.savedGenres = vm.boardingData.add_genre.join("@");
					}
					vm.boardingData = {}; // clear the data
                }, function(error){
                    deferred.resolve();
                });  
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        };

        vm.Date = function(date) {
			if (angular.isDefined(date) && date != null) {
				return new Date(date.replace(/-/g, '/'));
			} else {
				return new Date(date);
			}
        };

		// Set Movie Rating
        vm.setRating = function(movie_id, rating) {
			if (!vm.selectedMovieListObj[movie_id]) {
				vm.selectedMovieListObj[movie_id] = rating;
				if(rating > 2) {
					vm.recommendedRating++;
				}
				OnBoardingService.addMovieCheckin(ftoken, sessionId, movie_id, rating).then(function(result) { 
				}, function(error){                
				});
				vm.checkForNavigationVisibility();
			}
        };

		// Prev / Next Buttons Availability
        vm.checkForNavigationVisibility = function() {
            switch(vm.currentStep) {
                case 1:
                    vm.skipButton = 0;
                    vm.prevButton = false;
                    if(vm.boardingData.add_language && vm.boardingData.add_language.length >0) {
                        vm.nextButton = true;    
                    } else {
                        vm.nextButton = false;
                    }
                    break;
                    
                case 2:
                    vm.prevButton = true;
                    vm.skipButton = 0; 
                    if(vm.boardingData.add_genre && vm.boardingData.add_genre.length >0) {
                        vm.nextButton = true;                            
                    } else {
                        vm.nextButton = false;
                    }
                    break;

                case 3:                                   
                    vm.prevButton = true;
                    if(vm.boardingData.add_actor && vm.boardingData.add_actor.length >0) {
                        vm.nextButton = true;    
                        vm.skipButton = 1;
                    } else {
                        vm.nextButton = false;
                        vm.skipButton = 2;
                    }
                    break;

                case 4:
                    vm.prevButton = true;
                    if(vm.boardingData.follow_critic && vm.boardingData.follow_critic.length >0) {
                        vm.nextButton = true;    
                        vm.skipButton = 1; 
                    } else {
                        vm.nextButton = false;
                        vm.skipButton = 2;
                    }
                    break;

                case 5:
                    vm.skipButton = 0;
                    vm.prevButton = false;     
                    if(vm.recommendedRating > 4) {
                        vm.nextButton = true;    
                    } else {
                        vm.nextButton = false;
                    }                                         
                    break;
            }
        }
		
		vm.loadMovieTrailer = function(movie_trailer) {
            vm.loadVideo = true;
            vm.movie_trailer = movie_trailer;
			
            angular.element('#introVideo').modal('show');
            angular.element('#introVideo').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
                vm.loadVideo = false;
            });
        };
    }

})();
