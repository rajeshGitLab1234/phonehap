/**
 * @ngdoc service
 * @name angularApp.service:OnBoardingService
 * @description
 * # OnBoardingService
 * OnBoarding Service for OnBoarding controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('OnBoardingService', OnBoardingService);

    OnBoardingService.$inject = ['$http', '$q', 'CONSTANTS'];

    function OnBoardingService($http, $q, CONSTANTS) {

        var service = {
            searchActor: searchActor,
            getRecommendedActor: getRecommendedActor,
            getRecommendedCritics: getRecommendedCritics,
            searchCritic: searchCritic,
            followCritic: followCritic,
            updateRecommendationCriterion: updateRecommendationCriterion,
            getFlicktreeRecommendedMovies: getFlicktreeRecommendedMovies,
            searchMovies: searchMovies,
            addMovieCheckin: addMovieCheckin,
			checkRecommendations: checkRecommendations
        };

        return service;

        function searchActor(ftoken, sessionId, searchTerm) {
            var url = CONSTANTS.API_URL+'&METHOD=SEARCH_ACTOR&FTOKEN='+ftoken+'&SESSION='+sessionId+'&start_from=0&max_results=5&search_term='+searchTerm;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchActor", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function searchMovies(ftoken, sessionId, searchTerm) {            
            var url = CONSTANTS.API_URL+'&METHOD=SEARCH_MOVIES&FTOKEN='+ftoken+'&SESSION='+sessionId+'&start_from=0&max_results=5&search_term='+searchTerm;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchMovies", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function searchCritic(ftoken, sessionId, searchTerm) {
            var url = CONSTANTS.API_URL+'&METHOD=SEARCH_CRITIC&FTOKEN='+ftoken+'&SESSION='+sessionId+'&start_from=0&max_results=5&search_term='+searchTerm;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchCritic", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function updateRecommendationCriterion(ftoken, sessionId, criterion_type, criterion_value) {
            var url = CONSTANTS.API_URL+'&METHOD=UPDATE_RECOMMENDATION_CRITERION&FTOKEN='+ftoken+'&SESSION='+sessionId+'&criterion_type='+criterion_type+'&criterion_value='+criterion_value;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("updateRecommendationCriterion", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function followCritic(ftoken, sessionId, critic_id) {
            var url = CONSTANTS.API_URL+'&METHOD=FOLLOW_CRITIC&FTOKEN='+ftoken+'&SESSION='+sessionId+'&critic_id='+critic_id;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("searchActor", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getFlicktreeRecommendedMovies(ftoken, sessionId, start_from, languages, genres) {
            //var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
            var url = CONSTANTS.API_URL+'&METHOD=GET_ONBOARDING_CHECKINS&FTOKEN='+ftoken+'&SESSION='+sessionId+'&movie_language='+languages+'&movie_genre='+genres+'&start_from='+start_from+'&max_results=30';
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getFlicktreeRecommendedMovies", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getRecommendedActor(ftoken, sessionId, pageNumber,language) {
            var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
            var url = CONSTANTS.API_URL+'&METHOD=GET_RECOMMENDED_ACTORS&FTOKEN='+ftoken+'&SESSION='+sessionId+'&language='+language+'&start_from='+start_from+'&max_results=30';
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getRecommendedActor", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getRecommendedCritics(ftoken, sessionId, pageNumber) {
            var start_from = (parseInt(pageNumber * 30) > 0) ? parseInt(pageNumber * 30) : 0;
            var url = CONSTANTS.API_URL+'&METHOD=GET_RECOMMENDED_CRITICS&FTOKEN='+ftoken+'&SESSION='+sessionId+'&start_from='+start_from+'&max_results=30';
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getRecommendedCritics", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function addMovieCheckin(ftoken, sessionId, movie_id, rating) {
            var url = CONSTANTS.API_URL+'&METHOD=ADD_CHECKIN&FTOKEN='+ftoken+'&SESSION='+sessionId+'&movie_id='+movie_id+'&checkin_rating='+rating;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("addMovieCheckin", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
        
		function checkRecommendations(ftoken, sessionId) {
            var url = CONSTANTS.API_URL+'&METHOD=GET_RECOMMENDATIONS&FTOKEN='+ftoken+'&SESSION='+sessionId;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("checkRecommendations", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();
