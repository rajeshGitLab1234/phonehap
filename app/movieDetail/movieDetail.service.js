/**
 * @ngdoc service
 * @name angularApp.service:MovieDetailService
 * @description
 * # MovieDetailService
 * Movie Detail Service for Movie Detail controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('MovieDetailService', MovieDetailService);

    MovieDetailService.$inject = ['$http', '$q', 'CONSTANTS'];

    function MovieDetailService($http, $q, CONSTANTS) {

        var service = {
            getCriticsData: getCriticsData,
            getMoviesData: getMoviesData,
            getUserReviewData: getUserReviewData,
			manageWatchList: manageWatchList,
            postReview: postReview,
            getCastDetail: getCastDetail,
            shareMovie: shareMovie,
            getMovieWatchData: getMovieWatchData,
			getCheckins: getCheckins
        };

        return service;

        function getMoviesData(sessionId, ftoken, movieId) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_MOVIE_DETAIL&movie_id='+movieId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getUserReviewData(sessionId, ftoken, movieId, startFrom) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_MOVIE_USERS_REVIEWS&start_from='+startFrom+'&movie_id='+movieId;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getUserReviewData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }        

        function getCriticsData(sessionId, ftoken, movieId, startFrom) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_MOVIE_REVIEWS&start_from='+startFrom+'&movie_id='+movieId;
			//console.log("URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCriticsData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		function manageWatchList(sessionId, ftoken, movieId, actionType) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&movie_id='+movieId;
			if (actionType == "add") {
				url += '&METHOD=ADD_WATCHLIST';
			} else {
				url += '&METHOD=DELETE_WATCHLIST';
			}
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("addToWatchList", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

		// Post Review or Add Checkin
		function postReview(sessionId, ftoken, movieId, reviewObj) {
			var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId;
			if (reviewObj.text.length > 0) {
				var reviewText = reviewObj.text.replace(/\s/g, "+");
				url += '&METHOD=ADD_USER_REVIEW&movie_id='+movieId+'&review_title='+reviewText+'&review_text='+reviewText+'&rating='+reviewObj.rating;
			} else {
				url += '&METHOD=ADD_CHECKIN&movie_id='+movieId+'&checkin_rating='+reviewObj.rating;
			}
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("postReview", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise; 
        }
		
        function getCastDetail(name) {           

            var nameText = name.replace(/\s/g, "+");
            var url = CONSTANTS.SITE_URL+'get_actors_json.php?term='+nameText;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCastDetail", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise; 

        }

        function shareMovie(sessionId, ftoken, movieId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=SHARE_MOVIE&movie_id='+movieId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareMovie", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }

        function getMovieWatchData(sessionId, ftoken, movieId) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_MOVIE_WATCH&movie_id='+movieId;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getMoviesData", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        } 

		function getCheckins(sessionId, ftoken) {
            var url = CONSTANTS.API_URL+'&SESSION='+sessionId+'&FTOKEN='+ftoken+'&METHOD=GET_USER_CHECKIN&max_results=1000';
			//console.log("getCheckins URL:" + url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("getCheckins", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;            
        }
    }

})();
