 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider
        .state('movie-detail', {
			url: 'movie-detail.html/:movieid',
            templateUrl: 'app/movieDetail/movieDetail.html',
            controller: 'MovieDetailController',
            controllerAs: 'movieDetailCtrl'
        });
        
        $urlRouterProvider.otherwise(function($injector,$location) {
          var $state = $injector.get('$state');          
            $state.go('movie-detail', {}, { location: false });
			if($location.path() && $location.path().split('/') && $location.path().split('/').length > 2) {
                $state.go('movie-detail', {movieid: $location.path().split('/')[2]}, { location: false });
            } else {
                $state.go('movie-detail', {}, { location: false });
            }
        });
    }
	
	angular
        .module('angularApp')
        .config(function(toastrConfig) {
		  angular.extend(toastrConfig, {
			positionClass: 'toast-bottom-center',
		  });
		});

})();