/**
 * @ngdoc controller
 * @name angularApp.controller:MovieDetailController
 * @description
 * # MovieDetailController
 * Movie Detail Controller loads Movie detail page data
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('MovieDetailController', MovieDetailController);

    MovieDetailController.$inject = ['MovieDetailService', 'StorageUtil', '$location', '$loading', '$sce', '$q', '$stateParams', '$window', '$filter', 'CONSTANTS', 'UserAuthService', 'toastr'];

    function MovieDetailController(MovieDetailService, StorageUtil, $location, $loading, $sce, $q, $stateParams, $window, $filter, CONSTANTS, UserAuthService, toastr) {
    	var vm = this;
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}   
		var movieId = ($stateParams.movieid != '') ? $stateParams.movieid : angular.element('body').data('id');
        //var movieId = angular.element('body').data('id');
        vm.loadVideo = false;
        vm.reviewObj = {
            rating: 0,
            text: ''
        };
		
		vm.myreviewObj = {
            rating: 0,
            text: ''
        };
        vm.isReadOnly = false;
		
		vm.currentLastCriticReview = 0;
		vm.lastLastCriticReview = -1;
		vm.currentLastUserReview = 0;
		vm.lastLastUserReview = -1;
		vm.isStillLoading = false;
		vm.activeTab = "criticReview";
		
        vm.loadCritics = function() {
            
            $loading.start('commonLoader');

            MovieDetailService.getMoviesData(sessionId, ftoken, movieId).then(function(result) {
                vm.movies = result.RESPONSE;
				// Get Movie Watch Availability
                MovieDetailService.getMovieWatchData(sessionId, ftoken, movieId).then(function(result) {
                    vm.movieWatchData = [];
                    if(result.RESPONSE && result.RESPONSE.length > 0) {
                        var freeMovieWatch = result.RESPONSE.filter(function(obj) {
                            if(obj.lowest_price === 'Free') {
                                return obj;
                            } 
                        }); 
                        var subscriptionMovieWatch = result.RESPONSE.filter(function(obj) {
                            if(obj.lowest_price === 'Subscription') {
                                return obj;
                            } 
                        }); 

                        var pricedMovieWatch = result.RESPONSE.filter(function(obj) {
                            if(obj.lowest_price !== 'Free' && obj.lowest_price !== 'Subscription') {
								if (obj.lowest_price.search(/\$/) < 0) {
									obj.lowest_price = 'Rs.' + Math.floor(obj.lowest_price);
								}
                                return obj;
                            } 
                        }); 

                        var sortedPriceMovies = pricedMovieWatch.sort(function(a, b) {
                            if(parseInt(a.lowest_price) > parseInt(b.lowest_price)) {
                                return 1;
                            } 
                            if(parseInt(a.lowest_price) < parseInt(b.lowest_price)) {
                                return -1;
                            }
                        });
                        if(freeMovieWatch && freeMovieWatch.length >0) {
                            vm.movieWatchData = vm.movieWatchData.concat(freeMovieWatch);
                        }

                        if(sortedPriceMovies && sortedPriceMovies.length >0) {
                            vm.movieWatchData = vm.movieWatchData.concat(sortedPriceMovies);
                        }

                        if(subscriptionMovieWatch && subscriptionMovieWatch.length >0) {
                            vm.movieWatchData = vm.movieWatchData.concat(subscriptionMovieWatch);
                        }                         
                        //console.log(vm.movieWatchData);
                    }
					// Stop Video on hiding the Modal
					angular.element('#introVideoHome').on('hidden.bs.modal', function (e) {
						var src = angular.element('#CommonVideoPlayer').attr("src");
						angular.element('#CommonVideoPlayer').attr("src",src);
					});
					// Check if movie already checked in, if so get the rating
					if (vm.movies.done_checkin == 'Y') {
						MovieDetailService.getCheckins(sessionId, ftoken).then(function(result) {
							var checkedMovie = result.RESPONSE.filter(function(obj) {
								if(obj.movie_id == vm.movies.movie_id) {
									//console.log("found movie");
									return obj;
								} 
							})[0];
							//console.log(checkedMovie);
							if (checkedMovie) {
								//console.log("checkin rating:" + checkedMovie.checkin_rating);
								vm.myreviewObj.rating = checkedMovie.checkin_rating;
							}
						});
					}
					// Get Movie User Reviews Data
                    MovieDetailService.getUserReviewData(sessionId, ftoken, movieId, 0).then(function(result) {
                        vm.userReview = result.RESPONSE;
						vm.currentLastUserReview = vm.userReview.length;
						// Get Movie Critics Review Data
                        MovieDetailService.getCriticsData(sessionId, ftoken, movieId, 0).then(function(result) {
                            vm.critics = result.RESPONSE;
							vm.currentLastCriticReview = vm.critics.length;
                            vm.loadVideo = true;
                            var swiper = new Swiper('.swiper-container', {
                                pagination: '.swiper-pagination',
                                slidesPerView: 'auto',
                                paginationClickable: true,
                                spaceBetween: 5,
                                onClick: function(swiper, event) {
                                    if(angular.element(event.target).data('cast')) {
                                        var actorname = angular.element(event.target).data('cast');
                                        $window.open(CONSTANTS.SITE_LINK + CONSTANTS.PAGES.ACTOR_PROFILE + '/' + actorname);    
                                    }
                                }
                            });
							// Get Actor Pics
                            vm.getCastPics(vm.movies.movie_cast);
                            $loading.finish('commonLoader');
                        }, function(error){
                            $loading.finish('commonLoader');
                        });
                    }, function(error){
                        $loading.finish('commonLoader');
                    }); 
                }, function(error){
                    $loading.finish('commonLoader');
                })
            }, function(error){
                $loading.finish('commonLoader');
            });            
        };

		vm.setActiveTab = function(tabname) {
			//console.log("setting active tab to " + tabname);
			vm.activeTab = tabname;
		}
		vm.loadMoreCriticReviews = function($event) {
			if (vm.isStillLoading !== true && vm.activeTab == "criticReview") { // only do next page if prev one has loaded 
				//console.log("loadMoreCriticReviews:" + vm.currentLastCriticReview);
				if (vm.currentLastCriticReview > vm.lastLastCriticReview) { // check if there were any results last time
					vm.lastLastCriticReview = vm.currentLastCriticReview;
					vm.isStillLoading = true;
					$loading.start('commonLoader');
					//console.log("loadMoreCriticReviews:" + vm.currentLastCriticReview);
					MovieDetailService.getCriticsData(sessionId, ftoken, movieId, vm.currentLastCriticReview).then(function(result) {
                        vm.critics = vm.critics.concat(result.RESPONSE);
						vm.currentLastCriticReview = vm.critics.length;
						vm.isStillLoading = false;
						$loading.finish('commonLoader');
					}, function(error){
						$loading.finish('commonLoader');
					});	
				}
			}
        };
		
		vm.loadMoreUserReviews = function($event) {
			if (vm.isStillLoading !== true && vm.activeTab == "userReview") { // only do next page if prev one has loaded 
				//console.log("loadMoreUserReviews:" + vm.currentLastUserReview);
				if (vm.currentLastUserReview > vm.lastLastUserReview) { // check if there were any results last time
					vm.lastLastUserReview = vm.currentLastUserReview;
					vm.isStillLoading = true;
					$loading.start('commonLoader');
					//console.log("loadMoreUserReviews:" + vm.currentLastUserReview);
					MovieDetailService.getUserReviewData(sessionId, ftoken, movieId, vm.currentLastUserReview).then(function(result) {
                        vm.userReview = vm.userReview.concat(result.RESPONSE);
						vm.currentLastUserReview = vm.userReview.length;
						vm.isStillLoading = false;
						$loading.finish('commonLoader');
					}, function(error){
						$loading.finish('commonLoader');
					});		
				}
			}
        };
		
		
        vm.Date = function(date) {
			if (angular.isDefined(date) && date != null) {
				return new Date(date.replace(/-/g, '/'));
			} else {
				return new Date(date);
			}
        };

        vm.expandWatchList = function() {
            angular.element('#watchList').toggleClass('fullWidth');
			vm.movieWatchExpanded = (vm.movieWatchExpanded) ? false : true;
        }

        vm.range = function(n) {
            return new Array(parseInt(n));
        };

		vm.rangeRemainder = function(n) {
            return new Array(parseInt(n));
        };
		
        vm.getFormattedRating = function(rating) {
			return (rating) ? parseFloat(rating/2).toFixed(1) : '--';
        };

		vm.manageWatchList = function(isInWatchList) {
            $loading.start('commonLoader');
			if (isInWatchList == 'Y') {
				MovieDetailService.manageWatchList(sessionId, ftoken, movieId, "delete").then(function(result) {
					$loading.finish('commonLoader');
					vm.movies.on_watchlist = "N";
					toastr.success(result.RESPONSE);
				});
			} else {
				MovieDetailService.manageWatchList(sessionId, ftoken, movieId, "add").then(function(result) {
					$loading.finish('commonLoader');
					vm.movies.on_watchlist = "Y";
					toastr.success(result.RESPONSE);
				});
			}            
        };
		
		vm.manageShare = function(isShared) {
			if (isShared != 'Y') {
				$loading.start('commonLoader');
				MovieDetailService.shareMovie(sessionId, ftoken, movieId).then(function(result) {
					$loading.finish('commonLoader');
					vm.movies.is_shared = "Y";
					toastr.success(result.RESPONSE);
				});
			}             
        };
		
        vm.postReviewForm = function() {
            if(vm.reviewObj.rating == 0) {
                alert("please select rating.");
            } else {
				vm.myreviewObj.rating = vm.reviewObj.rating;
				vm.movies.done_checkin = 'Y';
				//console.log("updating rating:" + vm.myreviewObj.rating);
                MovieDetailService.postReview(sessionId, ftoken, movieId, vm.reviewObj).then(function(result) {
                    toastr.success(result.RESPONSE);
                    MovieDetailService.getUserReviewData(sessionId, ftoken, movieId).then(function(result) {
                        vm.userReview = result.RESPONSE;
                        $loading.finish('commonLoader');
                    }, function(error){
                        $loading.finish('commonLoader');
                    });
                    angular.element('#writeReview').modal('hide');
                });    
            }
        };

        vm.openReviewBox = function(text,rating) {
            vm.isReadOnly = true;
            vm.reviewObj.text = text;
			vm.reviewObj.rating = rating;
            angular.element('#writeReview').modal('show');
        };

        vm.openWriteReviewBox = function() {
            vm.isReadOnly = false;
            vm.reviewObj = {
                rating: vm.myreviewObj.rating,
                text: ''
            };
            angular.element('#writeReview').modal('show');
        };

        vm.setRating = function(rating) {
            vm.reviewObj.rating = rating;
        };

        vm.trustSrcurl = function(data) {            
            return $sce.trustAsResourceUrl("//www.youtube.com/embed/" + data);
        };

        vm.toggleDescription = function() {
            angular.element('#movie_desc').toggleClass('SYNOPSIS SYNOPSIS-auto', 'slow', "easeOutSine");
			angular.element('#movie_desc_icon').toggleClass('glyphicon-plus glyphicon-minus');
        };

        vm.getCastPics = function(movie_cast){
            if(movie_cast && movie_cast.length>0) {
                var serviceParamsArray = movie_cast.map(function(str){
                    return str.replace(/\s/g, "+");
                });
                var serviceParams = serviceParamsArray.join('@@');

                MovieDetailService.getCastDetail(serviceParams).then(function(result) {
                    result.forEach(function(obj) {
                        $("div[data-cast='" + obj.actor_name +"'] img").attr('src', obj.actor_pic);
                    });                
                }); 
            }
       
        }

		vm.loadMovieTrailer = function() {
            vm.loadVideo = true;
            angular.element('#introVideo').modal('show');
            angular.element('#introVideo').on('hidden.bs.modal', function (e) {
				var src = angular.element('#VideoPlayer').attr("src");
				angular.element('#VideoPlayer').attr("src",src);
				vm.loadVideo = false;
            });
        };
		
		vm.shareMovieOnFacebook = function() {
			var URL = 'http://www.flickstree.com/create_share.php?id=' + vm.movies.movie_id + '&user=' + ftoken + '&type=movie';
			//console.log(URL);
			FB.ui({
			  method: 'feed',
			  picture: URL,
			  link: 'https://www.flickstree.com/movie-details?mid=' + vm.movies.movie_id,
			  privacy: {"value": "EVERYONE"}
			}, function() {
				vm.movies.is_shared = 'Y';
			});				
		};
		
        vm.loadCritics();
    }

})();
