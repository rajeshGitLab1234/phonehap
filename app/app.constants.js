/**
 * @ngdoc constants
 * @name angularApp.constant:CONSTANTS
 * @description
 * # CONSTANTS
 * CONSTANTS used for util App
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .constant('CONSTANTS', {
            
            API_URL: 'https://www.flickstree.com/api_service_130v.php?VERSION=1.30&ENCODING=JSON',
            SITE_URL: 'https://www.flickstree.com/',
            SITE_LINK: 'http://192.168.0.79:3000/',
            PAGES: {
            	ACTOR_PROFILE: 'actor-profile.html',
				DIRECTOR_PROFILE: 'director-profile.html',
                TIMELINE: 'timeline.html',
                CALENDAR: 'movie-release-dates.html',
                CRITIC_PROFILE: 'critics-profile.html',
				USER_PROFILE: 'user-profile.html',
                MOVIE_CALENDAR: 'movie-release-dates.html',
				MERCHANT_PROFILE: 'watch-it-on.html',
				ON_BOARDING: 'getting-started.html',
				PREFERENCES: 'change-preference.html'
            }
        });
})();
