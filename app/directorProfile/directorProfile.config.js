 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider
        .state('director-profile', {
            url: 'director-profile.html/:directorname',
            templateUrl: 'app/directorProfile/directorProfile.html',
            controller: 'DirectorProfileController',
            controllerAs: 'DirectorProfileCtrl'
            
        });
        
        $urlRouterProvider.otherwise(function($injector, $location) {
          var $state = $injector.get('$state');  
          var $stateParams = $injector.get('$stateParams');
          if($location.path() && $location.path().split('/') && $location.path().split('/').length > 2) {
            $state.go('director-profile', {directorname: $location.path().split('/')[2]}, { location: false });
          } else {
            $state.go('director-profile', {}, { location: false });
          }
        });
    }
	
	angular
        .module('angularApp')
        .config(function(toastrConfig) {
		  angular.extend(toastrConfig, {
			positionClass: 'toast-bottom-center',
		  });
		});

})();