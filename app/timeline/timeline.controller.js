/**
 * @ngdoc controller
 * @name angularApp.controller:TimelineController
 * @description
 * # TimelineController
 * Timeline Controller to load timeline details
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('TimelineController', TimelineController);

    TimelineController.$inject = ['TimelineService', 'StorageUtil', '$location', '$loading', 'UserAuthService'];

    function TimelineController(TimelineService, StorageUtil, $location, $loading, UserAuthService) {
    	var vm = this;   
		var sessionId = '';
		var ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				console.log("got userObj");
				sessionId = userObj.sessionId
				ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
		
		vm.loadTimeLineUserRHA = function() {
            $loading.start('commonLoader');
            TimelineService.getUserRHS(ftoken, sessionId).then(function(result) {
                vm.userRHA = result.RESPONSE;
                //console.log(result);   // result is equal to response.data(from timeline.service.js)
                console.log(vm.userRHA);  // array of 10 objects
                //console.log(result.RESULT);
                //console.log(result.REQUEST);
                //console.log(vm.userRHA[0]);  //accessing first object
                //console.log(vm.userRHA[0].method);  //GET_USER_CHECKIN
                //console.log(vm.userRHA[7].method);  //GET_USER_CHECKIN
                //console.log(vm.userRHA[2].method);  //GET_FRIENDS_SHARED_CRITIC_REVIEWS
                //console.log(vm.userRHA[3].method);  //GET_FRIENDS_SHARED_MOVIES
                //console.log(vm.userRHA[4].method);  //GET_FRIENDS_SHARED_MOVIES
                //console.log(vm.userRHA[5].method);  //GET_FOLLOWED_CRITIC_REVIEWS
                //console.log(vm.userRHA[6].method);  //GET_FOLLOWED_CRITIC_REVIEWS
                //console.log(vm.userRHA[8].method);  //GET_FOLLOWED_CRITIC_REVIEWS
                //console.log(vm.userRHA[8].friend_pic);  //GET_FOLLOWED_CRITIC_REVIEWS
                //console.log(vm.userRHA[8]);  //GET_FOLLOWED_CRITIC_REVIEWS
                //console.log(vm.userRHA[8].friend_name);  //GET_FOLLOWED_CRITIC_REVIEWS
                //console.log(vm.userRHA[9].method);  //GET_FOLLOWED_CRITIC_REVIEWS
                //console.log(vm.userRHA[1].method);  //GET_FOLLOWED_CRITIC_REVIEWS
                $loading.finish('commonLoader');
            }, function(error){
                $loading.finish('commonLoader');
            });
        };

        vm.Date = function(date) {
            return new Date(date);
        };

        vm.shareReview = function(reviewId) {
            $loading.start('commonLoader');
            TimelineService.shareReview(sessionId, ftoken, reviewId).then(function(result) {
                $loading.finish('commonLoader');
                console.log(result.RESPONSE);
                alert(result.RESPONSE);
            });
        };
		
		vm.loadMovieTrailer = function(movie_trailer) {
            vm.loadVideo = true;
            vm.movie_trailer = movie_trailer;

            angular.element('#introVideo').modal('show');
            angular.element('#introVideo').on('hidden.bs.modal', function (e) {
                vm.loadVideo = false;
            });
        };
        vm.loadTimeLineUserRHA();
    }

})();
