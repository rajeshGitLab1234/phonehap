 /**
 * @ngdoc config
 * @name angularApp.config:appConfig
 * @description
 * # appConfig
 * app config manages the states and config related settings
 */

 (function() {
    'use strict';
    angular
        .module('angularApp')
        .config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $stateProvider
       
        .state('timeline', {
            url: '/',
            templateUrl: 'app/timeline/timeline.html',
            controller: 'TimelineController',
            controllerAs: 'TimelineCtrl'
        });
        
        $urlRouterProvider.otherwise(function($injector) {
          var $state = $injector.get('$state');          
            $state.go('timeline', {}, { location: false });
        });
    }

})();