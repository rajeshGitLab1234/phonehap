/**
 * @ngdoc service
 * @name angularApp.service:TimelineService
 * @description
 * # TimelineService
 * Timeline Service for Timeline controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('TimelineService', TimelineService);

    TimelineService.$inject = ['$http', '$q', 'CONSTANTS'];

    function TimelineService($http, $q, CONSTANTS) {

        var service = {

            getUserRHS: getUserRHS,
            shareReview: shareReview,
            addComment: addComment,
            getComment: getComment,
            deleteComment: deleteComment,
            updateComment: updateComment,
            getUserInfo: getUserInfo


           
        };

        return service;

        function getUserRHS(ftoken, sessionId, start_from) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_USER_RHS&start_from='+0;
            console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                console.log("getUserRHS", response);
                console.log("getUserRHS", response.data);
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }


        function getUserInfo(ftoken, sessionId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_USER_INFO';
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                console.log("getUserProfile", response.data);
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function shareReview(sessionId, ftoken, reviewId) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=SHARE_CRITIC_REVIEW&review_id='+reviewId;
            console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareReview service data:", response.data);
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function addComment(sessionId, ftoken, postID, post_owner_id ,comment) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=ADD_COMMENT&post_id ='+postID+'&post_owner_id='+post_owner_id+'&comment='+comment;
            console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareReview service data:", response.data);
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }


        function getComment(sessionId, ftoken,postID, post_owner_id) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=GET_COMMENTS&post_id ='+postID+'&post_owner_id='+post_owner_id;
            console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareReview service data:", response.data);
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }


        function deleteComment(sessionId, ftoken, postID, post_owner_id ,commentID) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=ADD_COMMENT&post_id ='+postID+'&post_owner_id='+post_owner_id+'&comment_id='+commentID;
            console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareReview service data:", response.data);
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }


        function updateComment(sessionId, ftoken, postID, post_owner_id ,commentID, comment) {
            var url = CONSTANTS.API_URL+'&FTOKEN='+ftoken+'&SESSION='+sessionId+'&METHOD=ADD_COMMENT&post_id ='+postID+'&post_owner_id='+post_owner_id+'&comment_id='+commentID+'&comment='+comment;
            console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("shareReview service data:", response.data);
                if(response && response.data) {
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;
        }

        
    }

})();