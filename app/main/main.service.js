/**
 * @ngdoc service
 * @name angularApp.service:MainService
 * @description
 * # MainService
 * Main Service for main controller
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .service('MainService', MainService);

    MainService.$inject = ['$http', '$q', 'CONSTANTS'];

    function MainService($http, $q, CONSTANTS) {

        var service = {
            globalSearch: globalSearch
        };
        
        function globalSearch(searchParam) {
            searchParam = searchParam.replace(/\s/g, "+");
            var url = CONSTANTS.SITE_URL+'get_combined_json.php?term='+searchParam;
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                //console.log("globalSearch", response.data);
                if(response && response.data) {                    
                    deferred.resolve(response.data);
                }
            }, function(error) {
                console.log(error);
                deferred.reject(error);
            });
            return deferred.promise;  
        }

        return service;
    }

})();
