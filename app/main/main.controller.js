/**
 * @ngdoc controller
 * @name angularApp.controller:MainController
 * @description
 * # MainController
 * Main Controller to manage the base app stucture and side menu
 */

(function() {
    'use strict';

    angular
        .module('angularApp')
        .controller('MainController', MainController);

    MainController.$inject = ['MainService', 'StorageUtil', '$location', "$state", "$window", "$loading", "CONSTANTS", "$timeout", "UserAuthService"];

    function MainController(MainService, StorageUtil, $location, $state, $window, $loading, CONSTANTS, $timeout, UserAuthService) {
    	var vm = this;
		vm.sessionId = '';
		vm.ftoken = '';
		vm.isLogged = false;
		
		var userObj = UserAuthService.getUserInfo();
		if (userObj) {
			if (userObj.ftoken != '') {
				//console.log("MainController got userObj");
				vm.sessionId = userObj.sessionId
				vm.ftoken = userObj.ftoken; 
				vm.isLogged = true;
			}
		}
		
		//console.log("isLogged:" + vm.isLogged);
		
		var searchPromise = null; //$timeout(getRestDataFromServer, 2000);
		vm.searchmovies = [];
		vm.searchcritics = [];
		vm.searchactors = [];
		vm.searchdirectors = [];
		
    	angular.element(window).scroll(function () {    
			if(angular.element(this).scrollTop() > 5) {
				angular.element('#navbar-header').addClass('fixed-nav');
			} else {
				angular.element('#navbar-header').removeClass('fixed-nav');
			}
			if(angular.element(this).scrollTop() > 200) {
			    angular.element('#pageTop').show();
			} else {
			 	angular.element('#pageTop').hide();
			}
		});

		// Objective: To give a delay of 1s to typeahead search so as to give user time to complete the word
		// Capture Search Box Changed event
		vm.searchBoxChanged = function($event) {
			var searchValue = $event.currentTarget.value; // get the value
			if (searchPromise && searchPromise != null && searchValue.length > 0) { // check if there was earlier search and not yet fulfilled
				//console.log("timeout cancelled");
				$timeout.cancel(searchPromise); // cancel earlier timer
			}
			// create a new timer
			searchPromise = $timeout(function() { angular.element('#searchBox').addClass('searchOngoing');
												 MainService.globalSearch(searchValue).then(function(result) {
													//console.log(result);
													
													// get result into variables
													vm.searchmovies = vm.searchcritics = vm.searchactors = vm.searchmerchants = vm.searchpeople = vm.searchdirectors = [];
													for (var i = 0; i < result.length; i++) {
														if (result[i].movies) {
															vm.searchmovies = result[i].movies;
														} else if (result[i].critics) {
															vm.searchcritics = result[i].critics;
														} else if (result[i].actors) {
															vm.searchactors = result[i].actors;
														} else if (result[i].merchants) {
															vm.searchmerchants = result[i].merchants;
														} else if (result[i].persons) {
															vm.searchpeople = result[i].persons;
														} else if (result[i].directors) {
															vm.searchdirectors = result[i].directors;
														}
													}
													angular.element('#searchBox').removeClass('searchOngoing');
													searchPromise = null;
													}); 
												}, 1000); // 1 second delay
		}
		
		vm.clearSearch = function() {
			angular.element('#searchBox').val('');
			vm.searchmovies = vm.searchcritics = vm.searchactors = vm.searchmerchants = vm.searchpeople = vm.searchdirectors = [];
		};
		vm.searchRoutePage = function(type, typeId) {
			switch(type) {

				case 'movie':
					vm.routeToMovieDetail(typeId);
					break;

				case 'actor':
					$window.location.href = CONSTANTS.SITE_LINK + CONSTANTS.PAGES.ACTOR_PROFILE + '/' + typeId;
                    //$window.open(CONSTANTS.SITE_LINK + CONSTANTS.PAGES.ACTOR_PROFILE + '/' + typeId);
					break;
					
				case 'director':
					$window.location.href = CONSTANTS.SITE_LINK + CONSTANTS.PAGES.DIRECTOR_PROFILE + '/' + typeId;
					break;
					
				case 'critic':
					vm.routeToCriticProfile(typeId);
					break;
					
				case 'person':
					$window.location.href = CONSTANTS.SITE_LINK + CONSTANTS.PAGES.USER_PROFILE + '/' + typeId;
                    //$window.open(CONSTANTS.SITE_LINK + CONSTANTS.PAGES.USER_PROFILE + '/' + typeId);
					break;	
					
				case 'merchant':
					$window.location.href = CONSTANTS.SITE_LINK + CONSTANTS.PAGES.MERCHANT_PROFILE + '/' + typeId;
                    //$window.open(CONSTANTS.SITE_LINK + CONSTANTS.PAGES.MERCHANT_PROFILE + '/' + typeId);
					break;	
			}
		};

		vm.scrollTop = function() {
			$('body').animate({
		        scrollTop : 0
		    }, 500);
		};

		vm.launchExternalUrl = function(url) {
            window.open(url, '_blank');
        };

		vm.showLoginOptions = function() {
			//console.log("mainCtrl showLoginOptions");
			angular.element('#modalLoginGlobal').modal();
		}
		
		vm.showLoginBox = function() {
			//console.log("mainCtrl showLoginBox");
			angular.element('#myModalLoginwithFacebook').modal();
		}
		
		vm.doLogin = function(loginType) {
			//console.log("mainCtrl doLogin");
			if (loginType == "facebook") {
				UserAuthService.doFBLogin();
			} else if (loginType == "amazon") {
				UserAuthService.doAmazonLogin();
			} else if (loginType == "google") {
				UserAuthService.doGoogleLogin();
			}
		}
		
        vm.routeToMovieDetail =  function(seo_link,movie_id) {
            //$window.location.href = CONSTANTS.SITE_URL+ 'mobile'+ seo_link;
			//console.log("main controller seo_link:" + seo_link + ",movie_id:" + movie_id);
			if (angular.isDefined(movie_id)) {
				$window.location.href = 'movie-details.html' + '/' + movie_id;
			}else if (seo_link) {
				//var re = /movie-details\?mid=(.+)/;
				var res = seo_link.match(/movie-details\?mid=(.+)/);
				//console.log("result:" + JSON.stringify(res));
				if (res) {
					//console.log("going to res:" + res[1]);
					$window.location.href = 'movie-details.html' + '/' + res[1];
				} else {
					//console.log("going to seo link:" + seo_link);
					$window.location.href = CONSTANTS.SITE_URL + 'mobile' + seo_link;
				}
			} else {
				//console.log("going to movie_id :" + movie_id);
				$window.location.href = 'movie-details.html' + '/' + movie_id;
			}
        };

        vm.routeToCriticProfile =  function(criticId) {
            $window.location.href = CONSTANTS.SITE_LINK+ CONSTANTS.PAGES.CRITIC_PROFILE+ '/' + criticId;
        };

		vm.routeToUserProfile =  function(userId) {
			if (userId) {
				$window.location.href = CONSTANTS.SITE_LINK+ CONSTANTS.PAGES.USER_PROFILE+ '/' + userId;
			} else {
				$window.location.href = CONSTANTS.SITE_LINK+ CONSTANTS.PAGES.USER_PROFILE;
			}
        };
		
        vm.routeToTimelinePage =  function() {
            $window.location.href = CONSTANTS.SITE_LINK+ CONSTANTS.PAGES.TIMELINE;
        };

        vm.routeToPage = function(pageUrl) {
        	$window.location.href = CONSTANTS.SITE_LINK+ pageUrl;
        }

        vm.routeToMovieCalendarPage =  function() {
            $window.location.href = CONSTANTS.SITE_LINK+ CONSTANTS.PAGES.MOVIE_CALENDAR;
        };

        vm.routeToHomePage =  function() {
            $window.location.href = CONSTANTS.SITE_LINK;
        };
		
		vm.routeToOnBoarding = function() {
			$window.location.href = CONSTANTS.SITE_LINK+ CONSTANTS.PAGES.ON_BOARDING;
		}
		
		vm.routeToPreferences = function() {
			$window.location.href = CONSTANTS.SITE_LINK+ CONSTANTS.PAGES.PREFERENCES;
		}
		
		vm.renderHTML = function(html_code)
        {
            var decoded = angular.element('<textarea />').html(html_code).text();
            return decoded;
        };
		
		vm.playTrivia = function() {
			angular.element('#triviafToken').val(vm.ftoken);
			angular.element('#triviaSession').val(vm.sessionId);
			angular.element('#triviaModal').modal();
        };
		
		vm.openSideMenu = function() {
			if (angular.element('#navbarCollapse').hasClass('collapse')) {
				//console.log("openSideMenu expanding");
				angular.element('#navbarCollapse').removeClass('collapse');
			} else {
				angular.element('#navbarCollapse').addClass('collapse');
				//console.log("openSideMenu collapsing");
			}
		};
		
		vm.logOut = function() {
			if (userObj) {
				//if (userObj.loginType == "google") {
				//	UserAuthService.onGoogleSignOut();
				//} else {
					UserAuthService.onSignOut();
				//}
			}
		};
		
		vm.shareFlickstreeOnFacebook = function() {
			var promoCode = '';
			//console.log('shareFlickstree:' + promoCode);
			FB.ui({
			  method: 'feed',
			  picture: 'http://84259a2b0b2ccf59796b-d6601ebc417a9360d15fc2feef89f9ce.r66.cf1.rackcdn.com/img/logo200.jpg',
			  link: 'https://www.flickstree.com/discover-movies?' + promoCode,
			  privacy: {"value": "EVERYONE"}
			}, function() {});
		};
    }

})();
