/**
 * @ngdoc module.
 * @name angularApp
 * @description
 * # angularApp
 * Main module of the application 
 */


 (function() {
    'use strict';
    angular
        .module('angularApp', [
            'UtilApp',
            'lazy-scroll', 
            'ImageCropper'        
        ])
        .run(["$window", function($window) {
        	angular.element($window).bind('orientationchange', function (event) {
        		angular.element('body').removeClass('portrait');
        		angular.element('body').removeClass('landscape');
        		if(window.innerHeight > window.innerWidth) {
        			angular.element('body').addClass('portrait'); 
        		} else {
        			angular.element('body').addClass('landscape');  
        		}
			   
			});
        }]);
})();