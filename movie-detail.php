<?php
$flickstreelinks_cdn = "http://63d8f228c01f50f1046d-68ff8666d0147bd8f0d88b78e2d6fbab.r99.cf1.rackcdn.com";
$create_movie_link = "https://www.flickstree.com/create_movie_link.php?movie_name=";

if ($_REQUEST["country"] != "" && $_REQUEST["year"] != "" && $_REQUEST["title"] != "")
{
	$country = preg_replace("/[^\w\s]/","",trim($_REQUEST["country"]));
	$year = preg_replace("/[^\d]/","",trim($_REQUEST["year"]));
	$title = preg_replace("/[^a-zA-Z0-9\'\-]/","",trim($_REQUEST["title"]));
	
	$movie_detail = file_get_contents("$flickstreelinks_cdn/$country/$year/$title");
	if ($movie_detail != "")
	{
		if (preg_match("/movie\-details\?mid=(.+)/",$movie_detail,$matches)) {
			$mid = $matches[1];
			$movie_array = explode("::",file_get_contents("$flickstreelinks_cdn/movies/$mid"));
			if (count($movie_array) == 28)
			{
				$movie_name = $movie_array[3];
				$movie_stars = $movie_array[14];
				$movie_genres = $movie_array[27];
			}
			else if (count($movie_array) == 56)
			{
				$movie_name = $movie_array[6];
				$movie_stars = $movie_array[28];
				$movie_genres = $movie_array[54];
			}
		} else {
			echo $movie_detail;
			exit;
		}			
	} 
	else // no link available, try creating 
	{
		//echo "trying " . "$create_movie_link" . urlencode($title) . "<br>";
		$results = file_get_contents("$create_movie_link" . urlencode($title)); // try creating the links for the movie
		//echo "Results: $results<br>";
		$movie_detail = file_get_contents("$flickstreelinks_cdn/$country/$year/$title");
		if ($movie_detail != "")
		{
			if (preg_match("/movie\-details\?mid=(.+)/",$movie_detail,$matches)) {
				$mid = $matches[1];
			}
				
		} 
	}
}

//print_r($_COOKIE);


if ($ftoken == "") {
	$ftoken = $_COOKIE["ftoken"];
}

if ($session == "") {
	$session = $_COOKIE["accessToken"];
}
$login_type = $_COOKIE["login_type"];

if ($mid == "")
{
	echo "No Such Movie";
	exit;
}
?>

<!doctype html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flicktree</title>
	<base href="/mobile2/">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/lib/angular-toastr/dist/angular-toastr.min.css" />
    <link rel="stylesheet" href="assets/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/lib/angular-loading/angular-loading.css" />
    <link rel="stylesheet" href="assets/lib/Swiper/dist/css/swiper.min.css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- endbower -->
    <!-- endbuild -->
    <!-- build:css styles/main.css -->
        
    <!-- endbuild -->
</head>

<body ng-app="angularApp" ng-controller="MainController as mainCtrl" data-movieid="<?php echo $mid?>">
     
    <nav class="navbar navbar-default">
        <div class="container-fluid"> 
            <!-- Brand and toggle get grouped for better mobile display -->
            <div id="navbar-header" class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
              <a class="navbar-brand" href="#"><span style="color:#cb202e;">F</span>T</a></div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="defaultNavbar1">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link<span class="sr-only">(current)</span></a></li>
                <li><a href="#">Link</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
              </ul>
            </div>
        <!-- /.navbar-collapse --> 
        </div>
        <!-- /.container-fluid --> 
    </nav>

    <div ui-view dw-loading="commonLoader"></div>

    <div id="pageTop" class="scrollBtn" ng-click="mainCtrl.scrollTop()">
        <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
    </div>

    <!-- <div ui-view dw-loading="commonLoader"></div> -->

    <script src="assets/lib/jquery/dist/jquery.min.js"></script>
    <script src="assets/lib/Swiper/dist/js/swiper.jquery.min.js"></script>
    <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/lib/angular/angular.min.js"></script>
    <script src="assets/lib/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="assets/lib/angular-toastr/dist/angular-toastr.tpls.js"></script>
    <script src="assets/lib/spin.js/spin.min.js"></script>
    <script src="assets/lib/angular-loading/angular-loading.js"></script>
    <script src="assets/lib/lazy-scroll.min.js"></script>
    <script src="assets/lib/image-crop.js"></script>

    <script src="app/utils/UtilApp.js"></script>
    <script src="app/utils/constants.js"></script>
    <script src="app/utils/errorHandler.dacorator.js"></script>
    <script src="app/utils/storageUtil.service.js"></script>
    <script src="app/utils/httpErrorHandler.service.js"></script>
    <script src="app/app.js"></script>
    <script src="app/app.movie.config.js"></script>
    <script src="app/app.constants.js"></script>
    <script src="app/shared/filters/commonFilter.js"></script>
    <script src="app/main/main.controller.js"></script>
    <script src="app/main/main.service.js"></script>
    <script src="app/home/home.controller.js"></script>
    <script src="app/home/home.service.js"></script>
    <script src="app/popularCritics/popularCritics.controller.js"></script>
    <script src="app/popularCritics/popularCritics.service.js"></script>
    <script src="app/criticsProfileFollow/criticsProfileFollow.controller.js"></script>
    <script src="app/criticsProfileFollow/criticsProfileFollow.service.js"></script>
    <script src="app/userProfile/userProfile.controller.js"></script>
    <script src="app/userProfile/userProfile.service.js"></script>
    <script src="app/discoverMovies/discoverMovies.controller.js"></script>
    <script src="app/discoverMovies/discoverMovies.service.js"></script>
    <script src="app/movieDetail/movieDetail.controller.js"></script>
    <script src="app/movieDetail/movieDetail.service.js"></script>
    <script src="app/actorProfile/actorProfile.controller.js"></script>
    <script src="app/actorProfile/actorProfile.service.js"></script>
    <script src="app/merchantProfile/merchantProfile.controller.js"></script>
    <script src="app/merchantProfile/merchantProfile.service.js"></script>
    <script src="app/onBoarding/onBoarding.controller.js"></script>
    <script src="app/onBoarding/onBoarding.service.js"></script>
    
</body>

</html>
